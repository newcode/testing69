<?php include 'includes/head.php'; ?>
<?php include 'includes/top.php'; ?>
<?php include 'includes/search.php'; ?>
<div class="page has-search" data-fixer>
    <div class="wrap">
        <div class="page__grid grid">
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/bali.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Bali</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#10dienų</a>
                            <a href="" class="grid__tag">#poilsinė</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price grid__price--old">&euro;1999</div>
                            <div class="grid__price grid__price--new"> &euro;1599</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Bus gerai</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/kinija.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Kinija</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#10dienų</a>
                            <a href="" class="grid__tag">#pažintinė</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price">&euro;1299</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item newsblock">
                <h3 class="newsblock__title">Naujienlaiškis</h3>
                <div class="newsblock__tags">
                    <a href="" class="newsblock__tag">#nuolaidos</a>
                    <a href="" class="newsblock__tag">#pasiūlymai</a>
                    <a href="" class="newsblock__tag">#naujienos</a>
                </div>
                <div class="newsblock__content">
                    <div class="newsblock__text">
                        <p>Užsisakyk AirGuru naujienlaiškį ir gauk geriausius kelionių pasiūlymus pirmas!</p>
                    </div>
                    <form class="form">
                        <div class="form__row">
                            Noriu gauti naujienas apie
                        </div>
                        <div class="form__row" data-form-parent>
                            <div class="form__item" data-form-item>
                                <label class="form__label">
                                    <input type="checkbox" class="form__input form__checkbox" name="newsletter_exotic" value="newsletter_exotic" data-form-input>
                                    <span class="form__label-content form__label-content--checkbox">Egzotines keliones</span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row" data-form-parent>
                            <div class="form__item" data-form-item>
                                <label class="form__label">
                                    <input type="checkbox" class="form__input form__checkbox" name="newsletter_recereational" value="newsletter_recereational" data-form-input>
                                    <span class="form__label-content form__label-content--checkbox">Poilsines keliones</span>
                                </label>
                            </div>
                        </div>
                        <div class="form__row" data-form-parent>
                            <div class="form__item" data-form-item>
                                <input type="email" class="form__text form__text--email form__input" name="newsletter_email" placeholder="Jūsų el. paštas" data-form-input required>
                            </div>
                        </div>
                        <div class="form__row" data-form-parent>
                            <div class="form__item" data-form-item>
                                <button type="submit" class="form__submit form__submit--newsletter form__input btn" data-form-input>Užsakyti</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/niujorkas.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Niujorkas</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">##8dienos</a>
                            <a href="" class="grid__tag">#pažintinė</a>
                            <a href="" class="grid__tag">#Amerija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price grid__price">&euro;1599</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Bus gerai</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/singapuras.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Singapūras</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#8dienos</a>
                            <a href="" class="grid__tag">#pažintinė</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price grid__price--old">&euro;1299</div>
                            <div class="grid__price grid__price--new">&euro;949</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/iranas.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Iranas</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#12dienų</a>
                            <a href="" class="grid__tag">#poilsinė</a>
                            <a href="" class="grid__tag">#vidurioazija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price">&euro;949</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Bus gerai</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/bali.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Bali</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#10dienų</a>
                            <a href="" class="grid__tag">#poilsinė</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price grid__price--old">&euro;1999</div>
                            <div class="grid__price grid__price--new">&euro;1599</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Bus gerai</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/kinija.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Kinija</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#10dienų</a>
                            <a href="" class="grid__tag">#pažintinė</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price">&euro;1299</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
            <div class="grid__item">
                <div class="grid__top" style="background-image: url('images/tailandas.png')">
                    <div class="grid__top-info">
                        <h3 class="grid__title">Tailandas</h3>
                        <div class="grid__tags">
                            <a href="" class="grid__tag">#19dienų</a>
                            <a href="" class="grid__tag">#mišri</a>
                            <a href="" class="grid__tag">#Azija</a>
                        </div>
                        <div class="grid__pricing">
                            <div class="grid__price">&euro;949</div>
                        </div>
                    </div>
                </div>
                <div class="grid__content">
                    <h5 class="grid__subtitle">Magiškasis Tailandas: apsilankymas Bankoke ir poilsis Krabi saloje</h5>
                    <ul class="grid__list">
                        <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                        <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                        <li class="grid__list-item">Gyvensite prie jūros</li>
                        <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                    </ul>
                    <div class="grid__more">
                        <span class="grid__more-text">Sužinok daugiau</span>
                        <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                        <a href="" class="grid__more-link"></a>
                    </div>
                    <div class="grid__likebox"></div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</div>
<?php include 'includes/foot.php'; ?>
