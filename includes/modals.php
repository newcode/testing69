<div class="modal js-first-modal" data-modal="first">
    <div class="modal__cont">
        <div class="modal__first first-modal">
            <div class="first-modal__wrap" style="background-image: url('images/dialog.png')">
                <div class="first-modal__cont">
                    <div class="first-modal__title">10 EUR</div>
                    <div class="first-modal__subtitle">nuolaida iškart</div>
                    <div class="first-modal__text">Būk greitesnis ir keliauk pigiau! Užsisakyk AirGuru kelionių pasiūlymus į savo el. paštą ir gauk 10 EUR kuponą!</div>
                    <form action="" class="form first-modal__form">
                        <div class="form__row first-modal__form-row" data-form-parent>
                            <div class="form__item first-modal__form-item" data-form-item>
                                <input type="email" class="form__text first-modal__email form__input" name="newsletter_email" placeholder="Jūsų el. paštas" data-form-input required>
                            </div>
                        </div>
                        <div class="form__row first-modal__form-row" data-form-parent>
                            <div class="form__item first-modal__form-item" data-form-item>
                                <button type="submit" class="form__submit first-modal__submit form__input btn" data-form-input>GAUTI 10 EUR</button>
                            </div>
                        </div>
                    </form>
                    <div class="first-modal__decline" data-modal-close>Ačiū, nuolaidos gauti nenoriu</div>
                    <div class="first-modal__close" data-modal-close>
                        <?php include 'img/close-box.svg'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" data-modal="gal1">
    <div class="modal__cont">
        <div class="modal__gallery gallery">
            <div class="gallery__swiper">
                <div class="swiper js-swiper">
                    <div class="swiper__wrapper swiper-wrapper">
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic1.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic2.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic3.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gallery__socials">
                        <div class="gallery__social">likebox</div>
                        <div class="gallery__social">
                            <div class="gallery__social-btn gallery__social-btn--mail">
                                <span class="gallery__social-icon"><?php include 'img/social-mail.svg'; ?></span>
                                <span class="gallery__social-text">Email</span>
                            </div>
                        </div>
                        <div class="gallery__social">
                            <div class="gallery__social-btn gallery__social-btn--messenger">
                                <span class="gallery__social-icon"><?php include 'img/social-messenger.svg'; ?></span>
                                <span class="gallery__social-text">Messenger</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper__close" data-modal-close>
                    <?php include 'img/close-box.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--prev swiper-button-prev" data-modal-close-prevent>
                    <?php include 'img/chevron-left.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--next swiper-button-next" data-modal-close-prevent>
                    <?php include 'img/chevron-right.svg'; ?>
                </div>
            </div>
            <div class="gallery__thumbs" data-gallery-thumbs data-modal-close-prevent>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic1.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic2.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic3.png')"></div>
                <div class="gallery__switch" data-gallery-switch>
                    <span class="gallery__switch-icon visible"><?php include 'img/eye-slash.svg'; ?></span>
                    <span class="gallery__switch-icon hidden"><?php include 'img/eye.svg'; ?></span>
                    <span class="gallery__switch-label visible">Hide</span>
                    <span class="gallery__switch-label hidden">Show</span>
                    <span class="gallery__switch-text">photo thumbs</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" data-modal="gal2">
    <div class="modal__cont">
        <div class="modal__gallery gallery">
            <div class="gallery__swiper">
                <div class="swiper js-swiper">
                    <div class="swiper__wrapper swiper-wrapper">
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic4.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic5.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic6.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic7.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper__close" data-modal-close>
                    <?php include 'img/close-box.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--prev swiper-button-prev" data-modal-close-prevent>
                    <?php include 'img/chevron-left.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--next swiper-button-next" data-modal-close-prevent>
                    <?php include 'img/chevron-right.svg'; ?>
                </div>
            </div>
            <div class="gallery__thumbs" data-gallery-thumbs data-modal-close-prevent>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic4.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic5.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic6.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic7.png')"></div>
                <div class="gallery__switch" data-gallery-switch>
                    <span class="gallery__switch-icon visible"><?php include 'img/eye-slash.svg'; ?></span>
                    <span class="gallery__switch-icon hidden"><?php include 'img/eye.svg'; ?></span>
                    <span class="gallery__switch-label visible">Hide</span>
                    <span class="gallery__switch-label hidden">Show</span>
                    <span class="gallery__switch-text">photo thumbs</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" data-modal="gal3">
    <div class="modal__cont">
        <div class="modal__gallery gallery">
            <div class="gallery__swiper">
                <div class="swiper js-swiper">
                    <div class="swiper__wrapper swiper-wrapper">
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic3.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic2.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper__close" data-modal-close>
                    <?php include 'img/close-box.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--prev swiper-button-prev" data-modal-close-prevent>
                    <?php include 'img/chevron-left.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--next swiper-button-next" data-modal-close-prevent>
                    <?php include 'img/chevron-right.svg'; ?>
                </div>
            </div>
            <div class="gallery__thumbs" data-gallery-thumbs data-modal-close-prevent>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic3.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic2.png')"></div>
                <div class="gallery__switch" data-gallery-switch>
                    <span class="gallery__switch-icon visible"><?php include 'img/eye-slash.svg'; ?></span>
                    <span class="gallery__switch-icon hidden"><?php include 'img/eye.svg'; ?></span>
                    <span class="gallery__switch-label visible">Hide</span>
                    <span class="gallery__switch-label hidden">Show</span>
                    <span class="gallery__switch-text">photo thumbs</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" data-modal="gal4">
    <div class="modal__cont">
        <div class="modal__gallery gallery">
            <div class="gallery__swiper">
                <div class="swiper js-swiper">
                    <div class="swiper__wrapper swiper-wrapper">
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic6.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic7.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic1.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper__close" data-modal-close>
                    <?php include 'img/close-box.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--prev swiper-button-prev" data-modal-close-prevent>
                    <?php include 'img/chevron-left.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--next swiper-button-next" data-modal-close-prevent>
                    <?php include 'img/chevron-right.svg'; ?>
                </div>
            </div>
            <div class="gallery__thumbs" data-gallery-thumbs data-modal-close-prevent>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic6.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic7.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic1.png')"></div>
                <div class="gallery__switch" data-gallery-switch>
                    <span class="gallery__switch-icon visible"><?php include 'img/eye-slash.svg'; ?></span>
                    <span class="gallery__switch-icon hidden"><?php include 'img/eye.svg'; ?></span>
                    <span class="gallery__switch-label visible">Hide</span>
                    <span class="gallery__switch-label hidden">Show</span>
                    <span class="gallery__switch-text">photo thumbs</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" data-modal="gal5">
    <div class="modal__cont">
        <div class="modal__gallery gallery">
            <div class="gallery__swiper">
                <div class="swiper js-swiper">
                    <div class="swiper__wrapper swiper-wrapper">
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic8.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic9.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic10.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic11.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic12.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                        <div class="swiper__slide swiper-slide" style="background-image: url('images/pic13.png')">
                            <div class="swiper__slide-info">
                                <div class="swiper__slide-title">
                                    Title goes here
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper__close" data-modal-close>
                    <?php include 'img/close-box.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--prev swiper-button-prev" data-modal-close-prevent>
                    <?php include 'img/chevron-left.svg'; ?>
                </div>
                <div class="swiper__nav swiper__nav--next swiper-button-next" data-modal-close-prevent>
                    <?php include 'img/chevron-right.svg'; ?>
                </div>
            </div>
            <div class="gallery__thumbs" data-gallery-thumbs data-modal-close-prevent>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic8.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic9.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic10.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic11.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic12.png')"></div>
                <div class="gallery__thumb js-swiper-thumb" style="background-image: url('images/pic13.png')"></div>
                <div class="gallery__switch" data-gallery-switch>
                    <span class="gallery__switch-icon visible"><?php include 'img/eye-slash.svg'; ?></span>
                    <span class="gallery__switch-icon hidden"><?php include 'img/eye.svg'; ?></span>
                    <span class="gallery__switch-label visible">Hide</span>
                    <span class="gallery__switch-label hidden">Show</span>
                    <span class="gallery__switch-text">photo thumbs</span>
                </div>
            </div>
        </div>
    </div>
</div>
