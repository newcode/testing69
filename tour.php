<?php include 'includes/head.php'; ?>
<?php include 'includes/top.php'; ?>
<div class="page page--tour" data-fixer>
    <div class="page__head" style="background-image: url('images/vietnamas.png')">
        <h1 class="page__title">Vietnamas</h1>
        <span class="page__subtitle">Daugiau apie Vietnamą</span>
    </div>
    <div class="page__content"
        data-v="app"
        data-v-order-base="1190"
        data-v-order-base-adults="1190"
        data-v-order-base-children="1090"
        data-v-order-base-babies="990"
        data-v-order-advance-ratio="0.2255"
        data-v-adults="3"
        data-v-children="0"
        data-v-babies="0"
        data-v-passengers-single-label="keleivis"
        data-v-passengers-multi-label="keleiviai">
        <div class="wrap">
            <form action="" class="form tour" data-scroll-parent>
                <div class="side tour__side">
                    <div class="side__cont tour__side-cont" data-scroll-fix>
                        <div class="side__details" data-order data-order-tour data-order-advance-ratio="0.2255">
                            <div class="side__detail">
                                <div class="side__label">
                                    Trukmė:
                                </div>
                                <div class="side__val">
                                    16 d.
                                </div>
                            </div>
                            <div class="side__detail">
                                <div class="side__label">
                                    Tipas:
                                </div>
                                <div class="side__val">
                                    <?php include 'img/recreation.svg'; ?>
                                </div>
                            </div>
                            <div class="side__detail">
                                <div class="side__label">
                                    Kaina asm.:
                                </div>
                                <div class="side__val">&euro;<span data-order-base v-text="orderBase.orderBaseAdults"></span></div>
                            </div>
                        </div>
                        <div class="side__info" data-tour-side>
                            <div class="tour__mob-stripe">
                                <div class="tour__mob-heading">Kelionės užsakymas</div>
                                <div class="tour__mob-close" data-tour-toggle>
                                    <div class="tour__mob-navicon">
                                    </div>
                                </div>
                            </div>
                            <div class="side__info-block side__info-block--list">
                                <div class="side__info-title">Šioje kelionėje Jūs:</div>
                                <ul class="side__list">
                                    <li class="side__lit-item">Aplankysite Ubud džiungles</li>
                                    <li class="side__lit-item">Pasimelsite Kho Bad šventykloje</li>
                                    <li class="side__lit-item">Gyvensite prie jūros</li>
                                    <li class="side__lit-item"> Jodinėsite gyvatėmis</li>
                                </ul>
                            </div>
                            <div class="side__info-block" data-form-parent>
                                <div class="side__info-title">Kelionės data</div>
                                <div class="form__item side__form-item" data-form-item>
                                    <select class="form__select form__input side__form-dates-select side__form-dates-input" name="checkout_sum" data-selectize data-selectize-dates data-form-input required>
                                        <option value="1190 1090 990">2016 01 21-2016 02 05 - €1190</option>
                                        <option value="2190 2090 1990">2016 01 21-2016 02 05 - €2190</option>
                                    </select>
                                </div>
                            </div>
                            <div class="side__info-block" data-form-parent>
                                <div class="side__info-title">Keleiviai</div>
                                <div class="form__item side__form-item" data-form-item>
                                    <input type="hidden" name="" value="1" data-form-input data-order-multiplier data-form-passenger-count>
                                    <div class="side__passengers " data-form-passengers>
                                        <div class="side__passengers-trigger" data-form-passengers-trigger>
                                            <span v-text="passengers"></span>&nbsp;<span v-text="passengerLabel"></span>
                                        </div>
                                        <div class="side__passengers-list">
                                            <div class="side__passengers-item">
                                                <div class="side__passengers-val" v-text="adults"></div>
                                                <div class="side__passengers-info">
                                                    <div class="side__passengers-label">Suaugę</div>
                                                    <div class="side__passengers-sublabel">16+ metų
                                                    </div>
                                                </div>
                                                <div class="side__passengers-actions">
                                                    <div class="side__passengers-action side__passengers-action--add" @click="addPerson('adults')"></div>
                                                    <div class="side__passengers-action side__passengers-action--remove" @click="removePerson('adults')"></div>
                                                </div>
                                            </div>
                                            <div class="side__passengers-item">
                                                <div class="side__passengers-val" v-text="children"></div>
                                                <div class="side__passengers-info">
                                                    <div class="side__passengers-label">Vaikai</div>
                                                    <div class="side__passengers-sublabel">2-11 metų</div>
                                                </div>
                                                <div class="side__passengers-actions">
                                                    <div class="side__passengers-action side__passengers-action--add" @click="addPerson('children')"></div>
                                                    <div class="side__passengers-action side__passengers-action--remove" @click="removePerson('children')"></div>
                                                </div>
                                            </div>
                                            <div class="side__passengers-item">
                                                <div class="side__passengers-val" v-text="babies"></div>
                                                <div class="side__passengers-info">
                                                    <div class="side__passengers-label">Kūdikiai</div>
                                                    <div class="side__passengers-sublabel">Iki 2 metų</div>
                                                </div>
                                                <div class="side__passengers-actions">
                                                    <div class="side__passengers-action side__passengers-action--add" @click="addPerson('babies')"></div>
                                                    <div class="side__passengers-action side__passengers-action--remove" @click="removePerson('babies')"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="side__info-block" data-order-services>
                                <div class="side__info-title">Papildomai</div>
                                <div class="side__order side__order--tour" v-if="orderServices[0].set">
                                    <div class="side__order-row side__order-row--tour">
                                        <div class="side__order-label side__order-label--tour">
                                            <span>Parkavimas Vilniaus oro uoste</span>
                                            <i class="side__order-clear side__order-clear--tour" @click="{setService(0)}"></i>
                                        </div>
                                        <div class="side__order-val side__order-val--tour">
                                            <span v-text="orderServices[0].passengers"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[0].priceTotal"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="side__order side__order--tour" v-if="orderServices[1].set">
                                    <div class="side__order-row side__order-row--tour">
                                        <div class="side__order-label side__order-label--tour">
                                            <span>Vienos dienos ekskursija į Ubud džiungles su vietiniais gidais</span>
                                            <i class="side__order-clear side__order-clear--tour" @click="{setService(1)}"></i>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="adults > 0">Suaugusieji:&nbsp;
                                            <span v-text="adults"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceAdults"></span>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="children > 0">Vaikai:&nbsp;
                                            <span v-text="children"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceChildren"></span>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="babies > 0">Kūdikiai:&nbsp;
                                            <span v-text="babies"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceBabies"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="side__order side__order--tour" v-if="orderServices[2].set">
                                    <div class="side__order-row side__order-row--tour">
                                        <div class="side__order-label side__order-label--tour">
                                            <span>Kelionė į fiktyvų turgų</span>
                                            <i class="side__order-clear side__order-clear--tour" @click="{setService(2)}"></i>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="adults > 0">Suaugusieji:&nbsp;
                                            <span v-text="adults"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceAdults"></span>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="children > 0">Vaikai:&nbsp;
                                            <span v-text="children"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceChildren"></span>
                                        </div>
                                        <div class="side__order-val side__order-val--tour" v-if="babies > 0">Kūdikiai:&nbsp;
                                            <span v-text="babies"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceBabies"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="side__total">
                                <div class="side__total-label">Dabar mokėti tik:</div>
                                <div class="side__total-val">&euro;<span v-text="orderAdvance"></span></div>
                            </div>
                            <div class="side__tour-submit">
                                <a href="" class="side__tour-btn btn">Pridėti keliautojų duomenis</a>
                            </div>
                        </div>
                    </div>
                </div>
                <h2 class="tour__title">Patirkite tikrą Vietnamietišką keliavimą: Nuo Himalajų iki Mekongo deltos. Neatrasti maršrutai ir keliai.</h2>
                <div class="tour__featured" style="background-image: url('images/map.png')"></div>
                <div class="tour__sections" data-sections>
                    <div class="tour__section">
                        <h4 class="tour__subtitle">1 Diena.  Įsikurime ir apžiūrėkime Hanojų.</h4>
                        <div class="tour__text">
                            <p>Norintiems daugiau pramogų patariama apsilankyti Siam Niramet teatre (už papildomą mokestį), kuriame kiekvieną dieną rengiami įspūdingi pasirodymai, atkartojantys tajų kultūrą ir istoriją per teatrą ir lydimi stulbinančių šviesos bei garso efektų. </p>
                        </div>
                        <div class="tour__pics">
                            <div class="tour__pic" data-modal-trigger="gal1" style="background-image: url('images/pic1.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal1" style="background-image: url('images/pic2.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal1" style="background-image: url('images/pic3.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tour__section">
                        <h4 class="tour__subtitle">2 Diena.  Pažinsime Sapą ir važiuosime naktiniu traukiniu.</h4>
                        <div class="tour__text">
                            <p>Norintiems daugiau pramogų patariama apsilankyti Siam Niramet teatre (už papildomą mokestį), kuriame kiekvieną dieną rengiami įspūdingi pasirodymai, atkartojantys tajų kultūrą ir istoriją per teatrą ir lydimi stulbinančių šviesos bei garso efektų. </p>
                        </div>
                        <div class="tour__pics">
                            <div class="tour__pic" data-modal-trigger="gal2" style="background-image: url('images/pic4.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal2" style="background-image: url('images/pic5.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal2" style="background-image: url('images/pic6.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal2" style="background-image: url('images/pic7.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tour__section">
                        <h4 class="tour__subtitle">1 Diena.  Įsikurime ir apžiūrėkime Hanojų.</h4>
                        <div class="tour__text">
                            <p>Norintiems daugiau pramogų patariama apsilankyti Siam Niramet teatre (už papildomą mokestį), kuriame kiekvieną dieną rengiami įspūdingi pasirodymai, atkartojantys tajų kultūrą ir istoriją per teatrą ir lydimi stulbinančių šviesos bei garso efektų. </p>
                        </div>
                        <div class="tour__pics">
                            <div class="tour__pic" data-modal-trigger="gal3" style="background-image: url('images/pic3.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal3" style="background-image: url('images/pic2.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tour__section">
                        <h4 class="tour__subtitle">2 Diena.  Pažinsime Sapą ir važiuosime naktiniu traukiniu.</h4>
                        <div class="tour__text">
                            <p>Norintiems daugiau pramogų patariama apsilankyti Siam Niramet teatre (už papildomą mokestį), kuriame kiekvieną dieną rengiami įspūdingi pasirodymai, atkartojantys tajų kultūrą ir istoriją per teatrą ir lydimi stulbinančių šviesos bei garso efektų. </p>
                        </div>
                        <div class="tour__pics">
                            <div class="tour__pic" data-modal-trigger="gal4" style="background-image: url('images/pic6.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal4" style="background-image: url('images/pic7.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                            <div class="tour__pic" data-modal-trigger="gal4" style="background-image: url('images/pic1.png')">
                                <div class="tour__pic-stretch"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tour__more btn btn--trans" data-toggle="sections">Peržiūrėti visą maršrutą</div>
                <div class="tour__blocks">
                    <div class="tour__block">
                        <div class="tour__gallery">
                            <h4 class="tour__subtitle">Galerija</h4>
                            <div class="tour__gallery-blocks">
                                <div class="tour__gallery-block" data-modal-trigger="gal5" style="background-image: url('images/pic8.png')"></div>
                                <div class="tour__gallery-block" data-modal-trigger="gal5" style="background-image: url('images/pic9.png')"></div>
                                <div class="tour__gallery-block" data-modal-trigger="gal5" style="background-image: url('images/pic10.png')"></div>
                                <div class="tour__gallery-block" data-modal-trigger="gal5" style="background-image: url('images/pic11.png')"></div>
                                <div class="tour__gallery-block" data-modal-trigger="gal5" style="background-image: url('images/pic12.png')"></div>
                                <div class="tour__gallery-block tour__gallery-block--more" data-modal-trigger="gal5" style="background-image: url('images/pic13.png')">
                                    <div class="tour__gallery-more">+5 nuotraukos</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tour__block">
                        <div class="tour__services">
                            <h2 class="tour__subtitle">Papildomai</h2>
                            <div class="tour__service-list">
                                <div class="tour__service service"
                                    data-v-service-set="false"
                                    data-v-service-multiply="false"
                                    data-v-service-price-adults="320"
                                    data-v-service-price-children="240"
                                    data-v-service-price-babies="160"
                                    :class="{'is-set': orderServices[0].set}">
                                    <div class="service__thumb" style="background-image: url('images/pic16.png')"></div>
                                    <div class="service__info">
                                        <h5 class="service__title" data-service-label>Parkavimas Vilniaus oro uoste</h5>
                                        <div class="service__cont">
                                            <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                        </div>
                                    </div>
                                    <div class="service__price"><span v-text="orderServices[0].priceAdults"></span> Eur</div>
                                    <div class="service__action" @click="{setService(0)}">
                                        <div class="btn service__btn-order">+ Užsakyti</div>
                                        <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                                    </div>
                                </div>
                                <div class="tour__service service"
                                    data-v-service-set="false"
                                    data-v-service-multiply="true"
                                    data-v-service-price-adults="320"
                                    data-v-service-price-children="240"
                                    data-v-service-price-babies="160"
                                    :class="{'is-set': orderServices[1].set}">
                                    <div class="service__thumb" style="background-image: url('images/pic14.png')"></div>
                                    <div class="service__info">
                                        <h5 class="service__title" data-service-label>Vienos dienos ekskursija į Ubud džiungles su vietiniais gidais</h5>
                                        <div class="service__cont">
                                            <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                        </div>
                                    </div>
                                    <div class="service__price"><span v-text="orderServices[1].priceAdults"></span> Eur</div>
                                    <div class="service__action" @click="{setService(1)}">
                                        <div class="btn service__btn-order">+ Užsakyti</div>
                                        <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                                    </div>
                                </div>
                                <div class="tour__service service"
                                    data-v-service-set="false"
                                    data-v-service-multiply="true"
                                    data-v-service-price-adults="320"
                                    data-v-service-price-children="240"
                                    data-v-service-price-babies="160"
                                    :class="{'is-set': orderServices[2].set}">
                                    <div class="service__thumb" style="background-image: url('images/pic15.png')"></div>
                                    <div class="service__info">
                                        <h5 class="service__title" data-service-label>Kelionė į fiktyvų turgų</h5>
                                        <div class="service__cont">
                                            <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                        </div>
                                    </div>
                                    <div class="service__price"><span v-text="orderServices[2].priceAdults"></span> Eur</div>
                                    <div class="service__action" @click="{setService(2)}">
                                        <div class="btn service__btn-order">+ Užsakyti</div>
                                        <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tour__block">
                        <div class="tour__terms clear">
                            <div class="tour__term">
                                <h4 class="tour__subtitle"><?php include 'img/inc.svg'; ?>Į kelionės kainą įskaičiuota:</h4>
                                <ul>
                                    <li>Skrydis Vilnius – Bergamo - Vilnius</li>
                                    <li>Registruotas 32 kg bagažas</li>
                                    <li>Orlaivio (10kg) bei rankinis bagažai</li>
                                    <li>Kelionė oro uostas- viešbutis -oro uostas</li>
                                    <li>7 naktys Bellavista 3* viešbutyje</li>
                                    <li>Maitinimas (pusryčiai ir vakarienė)</li>
                                </ul>
                            </div>
                            <div class="tour__term">
                                <h4 class="tour__subtitle"><?php include 'img/exc.svg'; ?>Į kelionės kainą neįskaičiuota:</h4>
                                <ul>
                                    <li>Vizos n shit</li>
                                    <li>Kelionės paslaugos</li>
                                    <li>Turisto nakvynės mokesčiai</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tour__block">
                        <div class="tour__comments">
                            <h4 class="tour__subtitle">Pasidalink įspūdžiais</h4>
                        </div>
                    </div>
                    <div class="tour__block">
                        <div class="tour__contact">
                            <h4 class="tour__subtitle">Pritaikyk kelionę Sau</h4>
                            <div class="tour__contact-text">Netinka datos ar norėtum pakoreguoti kelionės maršrutą?
                                <br/>Susisiek, mes atviri pasiūlymams!</div>
                            <div class="tour__contact-btn btn">susisiek su ekspertu</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="tour__grid">
            <h4 class="tour__grid-title">Jums taip pat galėtų patikti:</h4>
            <div class="wrap">
                <div class="page__grid grid">
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/bali.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Bali</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#10dienų</a>
                                    <a href="" class="grid__tag">#poilsinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price grid__price--old">&euro;1999</div>
                                    <div class="grid__price grid__price--new"> &euro;1599</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Bus gerai</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/kinija.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Kinija</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#10dienų</a>
                                    <a href="" class="grid__tag">#pažintinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price">&euro;1299</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/niujorkas.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Niujorkas</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">##8dienos</a>
                                    <a href="" class="grid__tag">#pažintinė</a>
                                    <a href="" class="grid__tag">#Amerija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price grid__price">&euro;1599</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Bus gerai</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/singapuras.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Singapūras</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#8dienos</a>
                                    <a href="" class="grid__tag">#pažintinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price grid__price--old">&euro;1299</div>
                                    <div class="grid__price grid__price--new">&euro;949</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/iranas.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Iranas</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#12dienų</a>
                                    <a href="" class="grid__tag">#poilsinė</a>
                                    <a href="" class="grid__tag">#vidurioazija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price">&euro;949</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Bus gerai</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/bali.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Bali</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#10dienų</a>
                                    <a href="" class="grid__tag">#poilsinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price grid__price--old">&euro;1999</div>
                                    <div class="grid__price grid__price--new">&euro;1599</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Bus gerai</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/kinija.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Kinija</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#10dienų</a>
                                    <a href="" class="grid__tag">#pažintinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price">&euro;1299</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Fantasmagoriškoji Kinija: pažintis su Pekinu, poilsis Dziangvė saloje ir greitasis traukinys į Sibirą</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/tailandas.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Tailandas</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#19dienų</a>
                                    <a href="" class="grid__tag">#mišri</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price">&euro;949</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Magiškasis Tailandas: apsilankymas Bankoke ir poilsis Krabi saloje</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                    <div class="grid__item">
                        <div class="grid__top" style="background-image: url('images/bali.png')">
                            <div class="grid__top-info">
                                <h3 class="grid__title">Bali</h3>
                                <div class="grid__tags">
                                    <a href="" class="grid__tag">#10dienų</a>
                                    <a href="" class="grid__tag">#poilsinė</a>
                                    <a href="" class="grid__tag">#Azija</a>
                                </div>
                                <div class="grid__pricing">
                                    <div class="grid__price grid__price--old">&euro;1999</div>
                                    <div class="grid__price grid__price--new"> &euro;1599</div>
                                </div>
                            </div>
                        </div>
                        <div class="grid__content">
                            <h5 class="grid__subtitle">Bus gerai</h5>
                            <ul class="grid__list">
                                <li class="grid__list-item">Aplankysite Ubud džiungles</li>
                                <li class="grid__list-item">Pasimelsite Kho Bad šventykloje</li>
                                <li class="grid__list-item">Gyvensite prie jūros</li>
                                <li class="grid__list-item">Jodinėsite gyvatėmis</li>
                            </ul>
                            <div class="grid__more">
                                <span class="grid__more-text">Sužinok daugiau</span>
                                <span class="grid__more-icon"><?php include 'img/more.svg'; ?></span>
                                <a href="" class="grid__more-link"></a>
                            </div>
                            <div class="grid__likebox"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</div>
<div class="tour__cta is-active" data-tour-toggle>
    <div class="tour__cta-btn btn is-active">Noriu užsakyti kelionę</div>
</div>
<?php include 'includes/foot.php'; ?>
