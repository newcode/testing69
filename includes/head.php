<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">

<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
    <meta charset="UTF-8">
    <title>AirGuru Egzotika</title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="img/fav/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/fav/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/fav/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,700i&subset=latin-ext" rel="stylesheet">


    <?php
  $dev = 1;
  if ($dev == 3) {
    require "vendor/less/Less.php";
      $parser = new Less_Parser();
      $parser->parseFile("frontend.less");
      echo '<style type="text/css">'.$parser->getCss().'</style>';
  } else if ($dev == 2) {
    require "vendor/less/Cache.php";
    $name = Less_Cache::Get(array('frontend.less'=>''),array('cache_dir'=>'cache','compress'=>true));
    $compiled = file_get_contents('cache/'.$name);
    echo '<style>'.$compiled.'</style>';
    file_put_contents('frontend.min.css',$compiled);
  } else if ($dev == 1) {
    echo '<link rel="stylesheet" type="text/css" href="frontend.css">';
    echo '<link rel="stylesheet" type="text/css" href="desk.css">';
  } ?>
</head>

<body>
