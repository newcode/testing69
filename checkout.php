<?php include 'includes/head.php'; ?>
<?php include 'includes/top.php'; ?>
<div class="page page--order" data-fixer>
    <div class="page__content" data-v="app" data-v-order-base="1190" data-v-order-base-adults="1190" data-v-order-base-children="1090" data-v-order-base-babies="990" data-v-order-advance-ratio="0.2255" data-v-adults="3" data-v-children="0" data-v-babies="0" data-v-passengers-single-label="keleivis" data-v-passengers-multi-label="keleiviai">
        <div class="wrap">
            <form action="" class="form order">
                <h2 class="order__title">Keleivių duomenys</h2>
                <div class="form__row order__form-row" data-form-parent data-passenger-type="adults">
                    <div class="form__row-title order__form-row-title">Suaugęs nr. <span data-passenger-count>1</span></div>
                    <div class="form__item order__form-item order__form-item--title" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Kreipinys</span>
                            <select class="form__select form__input order__form-select order__form-input" name="checkout_title1" data-selectize data-selectize-gender data-form-input required>
                                <option>Ponas</option>
                                <option>Ponia</option>
                            </select>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--name" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Vardas</span>
                            <input type="text" class="form__text form__input order__form-text" name="checkout_name1" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--surname" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Pavardė</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input1" name="checkout_surname1" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--date" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Gimimo data</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input order__form-input--calendar" name="checkout_date1" placeholder="MMMM mm dd" data-form-input data-form-date data-form-birthdate="checkout_date1" required>
                        </label>
                    </div>
                    <i class="order__remove-row" data-remove-row></i>
                </div>
                <div class="form__row order__form-row" data-form-parent data-passenger-type="adults">
                    <div class="form__row-title order__form-row-title">Suaugęs nr. <span data-passenger-count>2</span></div>
                    <div class="form__item order__form-item order__form-item--title" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Kreipinys</span>
                            <select class="form__select form__input order__form-select order__form-input" name="checkout_title2" data-selectize data-selectize-gender data-form-input required>
                                <option>Ponas</option>
                                <option>Ponia</option>
                            </select>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--name" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Vardas</span>
                            <input type="text" class="form__text form__input order__form-text" name="checkout_name2" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--surname" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Pavardė</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input" name="checkout_surname2" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--date" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Gimimo data</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input order__form-input--calendar" name="checkout_date2" placeholder="MMMM mm dd" data-form-input data-form-date data-form-birthdate="checkout_date2" required>
                        </label>
                    </div>
                    <i class="order__remove-row" data-remove-row></i>
                </div>
                <div class="form__row order__form-row" data-form-parent data-passenger-type="adults">
                    <div class="form__row-title order__form-row-title">Suaugęs nr. <span data-passenger-count>3</span></div>
                    <div class="form__item order__form-item order__form-item--title" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Kreipinys</span>
                            <select class="form__select form__input order__form-select order__form-input" name="checkout_title3" data-selectize data-selectize-gender data-form-input required>
                                <option>Ponas</option>
                                <option>Ponia</option>
                            </select>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--name" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Vardas</span>
                            <input type="text" class="form__text form__input order__form-text" name="checkout_name3" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--surname" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Pavardė</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input" name="checkout_surname3" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--date" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Gimimo data</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input order__form-input--calendar" name="checkout_date3" placeholder="MMMM mm dd" data-form-input data-form-date data-form-birthdate="checkout_date3" required>
                        </label>
                    </div>
                    <i class="order__remove-row" data-remove-row></i>
                </div>
                <div data-form-parent data-passenger-clone-cont></div>
                <div class="form__row order__form-row is-clone" data-form-parent data-passenger-clone>
                    <div class="form__row-title order__form-row-title"><span data-passenger-label></span>&nbsp;nr.&nbsp;<span data-passenger-count></span></div>
                    <div class="form__item order__form-item order__form-item--title" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Kreipinys</span>
                            <select class="form__select form__input order__form-select order__form-input" name="checkout_title1" data-selectize data-selectize-gender data-form-input required>
                                <option>Ponas</option>
                                <option>Ponia</option>
                            </select>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--name" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Vardas</span>
                            <input type="text" class="form__text form__input order__form-text" name="checkout_name1" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--surname" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Pavardė</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input1" name="checkout_surname1" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--date" data-form-item>
                        <label class="form__label order__form-label">
                            <span class="form__label-content order__form-label-content">Gimimo data</span>
                            <input type="text" class="form__text form__input order__form-text order__form-input order__form-input--calendar" name="checkout_date1" placeholder="MMMM mm dd" data-form-input data-form-date data-form-birthdate="checkout_date1" required>
                        </label>
                    </div>
                    <i class="order__remove-row" data-remove-row></i>
                </div>
                <div class="form__row order__form-row" data-form-parent>
                    <div class="form__row-title order__form-row-title">Pridėti keleivį:</div>
                    <div class="form__item order__form-item order__form-item--title" data-form-item>
                        <label class="form__label order__form-label">
                            <select class="form__select form__input order__form-select order__form-input" name="checkout_title3" data-selectize data-selectize-passenger-type data-form-input required>
                                <option value="adults">Suaugęs</option>
                                <option value="children">Vaikas</option>
                                <option value="babies">Kūdikis</option>
                            </select>
                        </label>
                    </div>
                    <div class="form__item order__form-item order__form-item--add" data-form-item>
                        <div class="btn" @click="addRow(passengerAddType)">+ Pridėti</div>
                    </div>
                </div>
                <div class="order__services">
                    <h2 class="order__subtitle">Papildomai</h2>
                    <div class="order__service-list">
                        <div class="order__service service" data-v-service-set="false" data-v-service-multiply="false" data-v-service-price-adults="320" data-v-service-price-children="240" data-v-service-price-babies="160" :class="{'is-set': orderServices[0].set}">
                            <div class="service__thumb" style="background-image: url('images/pic16.png')"></div>
                            <div class="service__info">
                                <h5 class="service__title">Parkavimas Vilniaus oro uoste</h5>
                                <div class="service__cont">
                                    <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                </div>
                            </div>
                            <div class="service__price"><span v-text="orderServices[0].priceAdults"></span> Eur</div>
                            <div class="service__action" @click="{setService(0)}">
                                <div class="btn service__btn-order">+ Užsakyti</div>
                                <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                            </div>
                        </div>
                        <div class="order__service service" data-v-service-set="false" data-v-service-multiply="true" data-v-service-price-adults="320" data-v-service-price-children="240" data-v-service-price-babies="160" :class="{'is-set': orderServices[1].set}">
                            <div class="service__thumb" style="background-image: url('images/pic14.png')"></div>
                            <div class="service__info">
                                <h5 class="service__title">Vienos dienos ekskursija į Ubud džiungles su vietiniais gidais</h5>
                                <div class="service__cont">
                                    <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                </div>
                            </div>
                            <div class="service__price"><span v-text="orderServices[1].priceAdults"></span> Eur</div>
                            <div class="service__action" @click="{setService(1)}">
                                <div class="btn service__btn-order">+ Užsakyti</div>
                                <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                            </div>
                        </div>
                        <div class="order__service service" data-v-service-set="false" data-v-service-multiply="true" data-v-service-price-adults="320" data-v-service-price-children="240" data-v-service-price-babies="160" :class="{'is-set': orderServices[2].set}">
                            <div class="service__thumb" style="background-image: url('images/pic15.png')"></div>
                            <div class="service__info">
                                <h5 class="service__title">Kelionė į fiktyvų turgų</h5>
                                <div class="service__cont">
                                    <p>Parkavimas naujoje daugiaaukštėje aikštelėje Vilniaus oro uoste. Rezervuokite internetu pigiau.</p>
                                </div>
                            </div>
                            <div class="service__price"><span v-text="orderServices[2].priceAdults"></span> Eur</div>
                            <div class="service__action" @click="{setService(2)}">
                                <div class="btn service__btn-order">+ Užsakyti</div>
                                <div class="service__btn-ordered">&nbsp;&nbsp;Užsakyta</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="side order__side">
                    <h2 class="order__side-title">Jūsų užsakymas</h2>
                    <div class="side__orders">
                        <div class="side__order">
                            <div class="side__order-row">
                                <div class="side__order-label">Kelionė į Vietamą</div>
                                <div class="side__order-val" v-cloack v-if="adults > 0">
                                    Suaugusieji:&nbsp;<span v-text="adults"></span>&nbsp;x&nbsp;&euro;<span v-text="orderBase.orderBaseAdults"></span>
                                </div>
                                <div class="side__order-val" v-cloack v-if="children > 0">
                                    Vaikai:&nbsp;<span v-text="children"></span>&nbsp;x&nbsp;&euro;<span v-text="orderBase.orderBaseChildren"></span>
                                </div>
                                <div class="side__order-val" v-cloack v-if="babies > 0">
                                    Kūdikiai:&nbsp;<span v-text="babies"></span>&nbsp;x&nbsp;&euro;<span v-text="orderBase.orderBaseBabies"></span>
                                </div>
                            </div>
                            <div class="side__order-row">
                                <div class="side__order-date">2016 01 21 - 2016 02 05</div>
                            </div>
                        </div>
                        <div class="side__order" v-if="orderServices[0].set" v-cloack>
                            <div class="side__order-row">
                                <div class="side__order-label">
                                    <span>Parkavimas Vilniaus oro uoste</span>
                                    <i class="side__order-clear" @click="{setService(0)}"></i>
                                </div>
                                <div class="side__order-val">
                                    <span v-text="orderServices[0].passengers"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[0].priceTotal"></span>
                                </div>
                            </div>
                        </div>
                        <div class="side__order" v-if="orderServices[1].set" v-cloack>
                            <div class="side__order-row">
                                <div class="side__order-label">
                                    <span>Vienos dienos ekskursija į Ubud džiungles su vietiniais gidais</span>
                                    <i class="side__order-clear" @click="{setService(1)}"></i>
                                </div>
                                <div class="side__order-val" v-if="adults > 0">Suaugusieji:&nbsp;
                                    <span v-text="adults"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceAdults"></span>
                                </div>
                                <div class="side__order-val" v-if="children > 0">Vaikai:&nbsp;
                                    <span v-text="children"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceChildren"></span>
                                </div>
                                <div class="side__order-val" v-if="babies > 0">Kūdikiai:&nbsp;
                                    <span v-text="babies"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[1].priceBabies"></span>
                                </div>
                            </div>
                        </div>
                        <div class="side__order" v-if="orderServices[2].set" v-cloack>
                            <div class="side__order-row">
                                <div class="side__order-label">
                                    <span>Kelionė į fiktyvų turgų</span>
                                    <i class="side__order-clear" @click="{setService(2)}"></i>
                                </div>
                                <div class="side__order-val" v-if="adults > 0">Suaugusieji:&nbsp;
                                    <span v-text="adults"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceAdults"></span>
                                </div>
                                <div class="side__order-val" v-if="children > 0">Vaikai:&nbsp;
                                    <span v-text="children"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceChildren"></span>
                                </div>
                                <div class="side__order-val" v-if="babies > 0">Kūdikiai:&nbsp;
                                    <span v-text="babies"></span>&nbsp;x&nbsp;&euro;<span v-text="orderServices[2].priceBabies"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="side__order-amounts">
                        <div class="side__order-amount">
                            <div class="side__order-amount-label">Visa kelionės kaina:</div>
                            <div class="side__order-amount-val">&euro;<span v-text="orderTotal"></span></div>
                        </div>
                        <div class="side__order-amount">
                            <div class="side__order-amount-label">Avansinis mokėjimas:</div>
                            <div class="side__order-amount-val">&euro;<span v-text="orderAdvance">900</span></div>
                        </div>
                    </div>
                    <div class="side__order-actions">
                        <div class="side__order-action" data-form-parent>
                            <div class="form__item side__form-item" data-form-item>
                                <label class="form__label side__form-label">
                                    <select class="form__select form__input side__form-select side__form-input" name="checkout_sum" data-selectize data-selectize-checkout data-form-input required>
                                        <option value="orderAdvance" v-text="orderAdvance"></option>
                                        <option value="orderTotal" v-text="orderTotal"></option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="side__order-action" data-form-parent>
                            <div class="form__item side__form-item" data-form-item>
                                <label class="form__label side__form-label">
                                    <input type="text" class="form__input form__text side__form-text" placeholder="Nuolaidos kodas">
                                    <button type="submit" class="form__submit btn" data-add-discount>+ Pridėti</button>
                                </label>
                            </div>
                        </div>
                        <div class="side__order-action" data-form-parent>
                            <div class="form__item side__form-item" data-form-item>
                                <button type="submit" class="form__submit side__form-submit btn">Apmokėti kelionę</button>
                            </div>
                        </div>
                    </div>
                    <div class="side__order-info">
                        <p>Šios avansinės įmokos pakanka, kad būtų patvirtinta rezervacija.</p>
                        <p>Likusią sumą galėsit apmokėti iki kovo 05 d.</p>
                        <p>užsakydami šią kelionę, jūs Sutinkate su kelionės sutarties <a href="">sąlygomis</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</div>
<?php include 'includes/foot.php'; ?>
