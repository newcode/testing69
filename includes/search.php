<form class="form search" data-search>
    <div class="search__mob-stripe">
        <div class="search__mob-heading">Paieška</div>
        <div class="search__mob-close" data-search-close>
            <div class="search__mob-navicon" data-search-close>
            </div>
        </div>
    </div>
    <div class="wrap">
        <h1 class="search__heading">Egzotinės kelionės</h1>
        <div class="form__row search__form-row">
            <div class="search__group search__group--types">
                <div class="search__pills" data-form-parent>
                    <div class="form__item search__form-item search__form-item--type" data-form-item>
                        <div class="search__parent-label">Kelionės tipas</div>
                        <label class="form__label search__form-label">
                            <span class="form__label-content form__label-content--checkbox search__label-content">
                            <span class="form__label-text search__label-img">
                                <?php include 'img/recreation.svg'; ?>
                            </span>
                            <span class="form__label-text search__label-text">Poilsinė</span>
                            </span>
                            <input type="checkbox" class="form__input form__checkbox search__form-input search__form-checkbox" name="type_recreation" data-form-input>
                        </label>
                    </div>
                    <div class="form__item search__form-item search__form-item--type" data-form-item>
                        <div class="search__parent-label">&nbsp;</div>
                        <label class="form__label search__form-label">
                            <span class="form__label-content form__label-content--checkbox search__label-content">
                            <span class="form__label-text search__label-img">
                                <?php include 'img/sightseeing.svg'; ?>
                            </span>
                            <span class="form__label-text search__label-text">Pažintinė</span>
                            </span>
                            <input type="checkbox" class="form__input form__checkbox search__form-input search__form-checkbox" name="type_sightseeing" data-form-input>
                        </label>
                    </div>
                    <div class="form__item search__form-item search__form-item--type" data-form-item>
                        <div class="search__parent-label">&nbsp;</div>
                        <label class="form__label search__form-label">
                            <span class="form__label-content form__label-content--checkbox search__label-content">
                            <span class="form__label-text search__label-img">
                                <?php include 'img/misc.svg'; ?>
                            </span>
                            <span class="form__label-text search__label-text">Mišri</span>
                            </span>
                            <input type="checkbox" class="form__input form__checkbox search__form-input search__form-checkbox" name="type_misc" data-form-input>
                        </label>
                    </div>
                    <div class="form__item search__form-item search__form-item--type" data-form-item>
                        <div class="search__parent-label">&nbsp;</div>
                        <label class="form__label search__form-label">
                            <span class="form__label-content form__label-content--checkbox search__label-content">
                            <span class="form__label-text search__label-img">
                                <?php include 'img/interesting.svg'; ?>
                            </span>
                            <span class="form__label-text search__label-text">Įdomi</span>
                            </span>
                            <input type="checkbox" class="form__input form__checkbox search__form-input search__form-checkbox" name="type_interesting" data-form-input>
                        </label>
                    </div>
                </div>
            </div>
            <div class="search__group search__group--dates">
                <div class="search__pills search__pills--date" data-form-parent data-form-date-parent>
                    <div class="form__item search__form-item search__form-item--date" data-form-item data-form-date-from-parent>
                        <div class="search__parent-label">išvykimas nuo:</div>
                        <label class="form__label search__form-label">
                            <input type="text" class="form__input form__text search__form-input search__form-text search__form-text--date" name="departure_from_date" data-form-input data-form-date data-form-date-from placeholder="Nesvarbu">
                        </label>
                    </div>
                    <div class="form__item search__form-item search__form-item--date" data-form-item data-form-date-to-parent>
                        <div class="search__parent-label">grįžimas iki:</div>
                        <label class="form__label search__form-label">
                            <input type="text" class="form__input form__text search__form-input search__form-text search__form-text--date" name="return_to_date" data-form-input data-form-date data-form-date-to placeholder="Nesvarbu">
                        </label>
                    </div>
                    <div class="search__form-clear" data-search-dates-clear>
                        <?php include 'img/times-circle.svg'; ?>
                    </div>
                </div>
            </div>
            <div class="search__group search__group--duration">
                <div class="search__pills" data-form-parent>
                    <div class="form__item search__form-item" data-form-item>
                        <div class="search__parent-label">kelionės trukmė:</div>
                        <div class="search__slider-cont" data-noui-parent>
                            <div class="search__slider" data-noui-slider="noui1" data-noui-min="1" data-noui-max="30" data-noui-start="2" data-noui-end="14" data-noui-step="1"></div>
                            <div class="search__slider-val search__slider-val--start" data-noui-start-val></div>
                            <div class="search__slider-val search__slider-val--end" data-noui-end-val></div>
                            <input type="hidden" name="min_duration" data-noui-min-input>
                            <input type="hidden" name="max_duration" data-noui-max-input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row search__form-row search__form-row--mobile">
            <div class="search__group search__group--main">
                <div class="search__pills" data-form-parent>
                    <div class="form__item search__form-item" data-form-item data-selectize-parent>
                        <select multiple class="form__input form__select search__form-input search__form-select" placeholder="Kur norite keliauti? Pvz. Azija, Indija, Bankokas ar pan." name="type_recreation" data-form-input data-selectize data-selectize-search>
                            <option value="Albania">Albania</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="Egyp">Egyp</option>
                            <option value="Greece">Greece</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Madeira">Madeira</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Oman">Oman</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                        </select>
                        <div class="search__select-above is-hidden" data-selectize-prepend>
                            <div class="search__subtitle">
                                <span class="search__subtitle-img">
                                <?php include 'img/heart.svg'; ?>
                                </span>
                                <span class="search__subtitle-text">POPULIARIOS PAIEŠKOS:</span>
                            </div>
                        </div>
                        <div class="search__select-bellow is-hidden" data-selectize-append data-selectable>
                            <div class="search__subtitle">
                                <span class="search__subtitle-img">
                                <?php include 'img/history.svg'; ?>
                                </span>
                                <span class="search__subtitle-text">Paskutinės PAIEŠKOS:</span>
                            </div>
                            <div class="search__history" data-selectize-history-items>
                            <div class="search__history-empty">Paskutinių paieškų nėra</div>
                            </div>
                            <button class="search__add btn" data-search-form-btn data-search-form-add>
                                <span class="search__add-text">Pridėti kryptis</span>
                                <i class="search__add-counter" data-search-form-add-counter>0</i>
                            </button>
                        </div>
                        <div class="search__clear" data-search-form-btn data-search-form-clear>
                            <?php include 'img/close.svg'; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search__group search__group--submit">
                <div class="search__pills" data-form-parent>
                    <div class="form__item search__form-item" data-form-item>
                        <button type="submit" class="form__submit btn search__submit" data-form-input data-search-submit>Ieškoti</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
