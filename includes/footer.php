<div class="footer">
    <div class="airlines mob-hide">
        <div class="wrap wrap--sm">
            <ul class="airlines__list">
                <li class="airlines__airline">
                    <?php include 'img/ryanair.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/wizzair.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/easyjet.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/norwegian.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/lufthansa.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/finnair.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
                <li class="airlines__airline">
                    <?php include 'img/airbaltic.svg'; ?>
                    <a href="" class="airlines__link"></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="payments mob-hide">
        <div class="wrap wrap--sm">
            <div class="payments__heading">Mokėjimo būdai</div>
            <ul class="payments__methods">
                <li class="payments__method payments__method--text">
                    <span class="payments__text">Grynaisiais arba kortele agentūroje</span>
                </li>
                <li class="payments__method payments__method--text">
                    <span class="payments__text">Banko pavedimu</span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                <img src="img/swedbank@2x.png" />
                </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/seb.svg'; ?>
                    </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/dnb.svg'; ?>
                    </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/danske.svg'; ?>
                    </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/visa.svg'; ?>
                    </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/mastercard.svg'; ?>
                    </span>
                </li>
                <li class="payments__method">
                    <span class="payments__img">
                    <?php include 'img/paypal.svg'; ?>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="wrap wrap--sm">
        <div class="footer__about mob-hide">
            <strong>AirGuru</strong> – sąžiningas lėktuvų bilietų portalas. Pigūs skrydžiai galutinėmis kainomis ir be jokių paslėptų mokesčių. Įkvepiantys pigių skrydžių pasiūlymai Tau. Išsirink, keliauk ir būk įkvėptas. Mes esame čia, kad tu būtum ten!&nbsp;Pigūs skrydžiai į 220 pasaulio šalių, tūkstančius oro uostų, šimtus kurortų ir dar daugiau.&nbsp;Sistema tenkinanti Jūsų poreikius: tiesioginiai, jungiamieji, tarpkontinentiniai ir bet kurie kiti, Jums reikalingi skrydžių pasiūlymai.&nbsp;Ypatingas <strong>AirGuru</strong> komandos dėmesys kiekvienam klientui: nuo pirmo kontakto iki lėktuvo bilieto kišenėje.&nbsp;Saugus ir paprastas atsiskaitymas elektronine bankininkyste internetu bei grynais pinigais arba mokėjimo kortele <strong>AirGuru</strong> biure.
        </div>
        <div class="footer__blocks">
            <div class="footer__block">
                <div class="footer__block-title mob-hide">Pagrindinė informacija</div>
                <ul class="footer__block-list">
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Apie mus</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Grupėms</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Sąlygos</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Verslui</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Pagalba</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Kontaktai</a>
                    </li>
                </ul>
            </div>
            <div class="footer__block mob-hide">
                <div class="footer__block-title">Kelionės su AirGuru</div>
                <ul class="footer__block-list">
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Poilsinės kelionės</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Pigūs skrydžiai</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Egzotinės kelionės</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Keliauk iš Varšuvos</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Pažintinės kelionės</a>
                    </li>
                </ul>
            </div>
            <div class="footer__block mob-hide">
                <div class="footer__block-title">AirGuru paslaugos</div>
                <ul class="footer__block-list">
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Viešbučiai</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Automobilių nuoma</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Slidinėjimo kelionės</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Vizos</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Kelionės draudimas</a>
                    </li>
                </ul>
            </div>
            <div class="footer__block mob-hide">
                <div class="footer__block-title">Skrydžių kryptys</div>
                <ul class="footer__block-list">
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Skrydžiai į Londoną</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Skrydžiai į Dubliną</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Skrydžiai į Paryžių</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Skrydžiai į Berlyną
                    </a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Skrydžiai į Barseloną</a>
                    </li>
                </ul>
            </div>
            <div class="footer__block mob-hide">
                <div class="footer__block-title">Naudinga keliaujantiems</div>
                <ul class="footer__block-list">
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Karščiausi pasiūlymai</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Pagalba</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Kelionės išsimokėtinai</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Dovanų kuponas</a>
                    </li>
                    <li class="footer__block-item">
                        <a href="" class="footer__block-link">Kelionių kryptys</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__copy">
        <div class="wrap wrap--sm clear">
            <div class="footer__copytext">&copy; 2016 AirGuru. Visos teisės saugomos</div>
            <div class="footer__social">
                <div class="footer__social-item">
                    <span class="footer__social-img">
                        <?php include 'img/facebook.svg'; ?>
                    </span>
                    <a href="" class="footer__social-link"></a>
                </div>
            </div>
        </div>
    </div>
</div>