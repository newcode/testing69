// SETUP GLOBAL OBJECT FOR GLOBAL NAMESPACING:
var ncApp = [];

// GLOBAL VARS:
var appendLib,
    checkUrl,
    checkMob,
    isMob,
    itTouch,
    debounce,
    addFont,
    winH,
    openModal,
    initSwipers,
    swiperLoaded,
    initSwiper,
    loadBirthCalendars,
    loadRangeCalendars,
    loadSelectizers,
    orderSelectizer,
    datesSelectizer,
    initRangeSliders,
    calcOrder,
    calcTour,
    calcScrollFix,
    searchCookie = {},
    searchHistoryLimit = 10,
    getSearchCookie,
    setSearchCookie,
    winW;

function calcScrollFix() {
    if ($('[data-scroll-fix]').length && !isMob) {
        var $scrollCont = $('[data-scroll-fix]'),
            scrollContWidth = $scrollCont.outerWidth(),
            scrollContHeight = $scrollCont.outerHeight(),
            scrollContLeft = $scrollCont.offset().left,
            $scrollParent = $('[data-scroll-parent]'),
            scrollParentHeight = $scrollParent.outerHeight(),
            scrollPos = $(window).scrollTop(),
            scrollParentTop = $scrollParent.offset().top,
            fixStart = $scrollParent.offset().top,
            fixEnd = scrollParentHeight - scrollContHeight + scrollParentTop,
            sideFixed = false,
            sideBottom = false;
        if (scrollPos > fixStart && scrollPos < fixEnd) {
            if (!sideFixed) {
                sideFixed = true;
                $scrollCont.addClass('is-fixed');
                $scrollCont.css({
                    width: scrollContWidth,
                    left: scrollContLeft
                });
            }
            if (sideBottom) {
                sideBottom = false;
                $scrollCont.removeClass('is-bottom');
            }
        } else if (scrollPos > fixStart && scrollPos > fixEnd) {
            if (!sideBottom) {
                sideBottom = true;
                $scrollCont.addClass('is-bottom');
            }
        } else if (sideFixed) {
            sideFixed = false;
            sideBottom = false;
            $scrollCont.removeClass('is-bottom');
            $scrollCont.removeClass('is-fixed');
            $scrollCont.removeAttr('style');
        }
        $(window).scroll(function(e) {
            scrollPos = $(window).scrollTop();
            if (scrollPos > fixStart && scrollPos < fixEnd) {
                if (!sideFixed) {
                    sideFixed = true;
                    $scrollCont.addClass('is-fixed');
                    $scrollCont.css({
                        width: scrollContWidth,
                        left: scrollContLeft
                    });
                }
                if (sideBottom) {
                    sideBottom = false;
                    $scrollCont.removeClass('is-bottom');
                }
            } else if (scrollPos > fixStart && scrollPos > fixEnd) {
                if (!sideBottom) {
                    sideBottom = true;
                    $scrollCont.addClass('is-bottom');
                }
            } else if (sideFixed) {
                sideFixed = false;
                sideBottom = false;
                $scrollCont.removeClass('is-bottom');
                $scrollCont.removeClass('is-fixed');
                $scrollCont.removeAttr('style');
            }
        });
    }
}

appendLib('//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js', function() {
    appendLib('//code.jquery.com/jquery-3.1.0.min.js', function() {
        appendLib('//cdnjs.cloudflare.com/ajax/libs/vue/2.0.3/vue.min.js', function() {
            appendLib('//cdnjs.cloudflare.com/ajax/libs/flexibility/1.0.6/flexibility.js', function() {
                window.onload = function() {
                    $(window).resize(setLayouts);
                    checkMob();
                    if ($('[data-v="app"]').length) {
                        // SET DEFAULT VALUES BEFORE LOAD
                        var $obj = $('[data-v="app"]'),
                            // orderBase = Number($obj.attr('data-v-order-base')),
                            // orderBase = [],
                            orderBaseAdults = Number($obj.attr('data-v-order-base-adults')),
                            orderBaseChildren = Number($obj.attr('data-v-order-base-children')),
                            orderBaseBabies = Number($obj.attr('data-v-order-base-babies')),
                            orderAdvanceRatio = Number($obj.attr('data-v-order-advance-ratio')),
                            orderBase = {
                                'orderBaseAdults': orderBaseAdults,
                                'orderBaseChildren': orderBaseChildren,
                                'orderBaseBabies': orderBaseBabies
                            },
                            adults = Number($obj.attr('data-v-adults')),
                            children = Number($obj.attr('data-v-children')),
                            babies = Number($obj.attr('data-v-babies')),
                            passengers = adults + children + babies,
                            orderServices = [],
                            service = {},
                            singleLabel = $obj.attr('data-v-passengers-single-label'),
                            multiLabel = $obj.attr('data-v-passengers-multi-label'),
                            passengerLabel = singleLabel,
                            $passengerClone = $('[data-passenger-clone]'),
                            $passengerCloneCont = $('[data-passenger-clone-cont]');
                        if (passengers > 1) {
                            passengerLabel = multiLabel;
                        }
                        // SET VALUES FOR EACH SERVICE
                        $('[data-v-service-set]').each(function() {
                            service = {};
                            service['priceAdults'] = Number($(this).attr('data-v-service-price-adults'));
                            service['priceChildren'] = Number($(this).attr('data-v-service-price-children'));
                            service['priceBabies'] = Number($(this).attr('data-v-service-price-babies'));
                            service['multiply'] = JSON.parse($(this).attr('data-v-service-multiply'));
                            service['set'] = JSON.parse($(this).attr('data-v-service-set'));
                            service['passengers'] = 1;
                            service['priceTotal'] = service['priceAdults'];
                            if (service['multiply']) {
                                service['passengers'] = passengers;
                                service['priceTotal'] = (service['priceAdults'] * adults) + (service['priceChildren'] * children) + (service['priceBabies'] * babies);
                            }
                            orderServices.push(service);
                        });
                        // STAT MAIN CALCULATION APP
                        ncApp.calcApp = new Vue({
                            data: {
                                orderBase: orderBase,
                                // orderBase: [],
                                orderBaseAdults: orderBaseAdults,
                                orderBaseChildren: orderBaseChildren,
                                orderBaseBabies: orderBaseBabies,
                                // orderBase: [
                                //     { 'orderBaseAdults': orderBaseAdults },
                                //     { 'orderBaseChildren': orderBaseChildren },
                                //     { 'orderBaseBabies': orderBaseBabies }
                                // ],
                                orderAdvanceRatio: orderAdvanceRatio,
                                adults: adults,
                                children: children,
                                babies: babies,
                                passengers: passengers,
                                orderServices: orderServices,
                                servicesPrice: 0,
                                // orderAdvance: orderAdvanceRatio,
                                orderAdvance: Math.round(orderAdvanceRatio * ((orderBase.orderBaseAdults * adults) + (orderBase.orderBaseChildren * children) + (orderBase.orderBaseBabies * babies))),
                                singleLabel: singleLabel,
                                passengerLabel: passengerLabel,
                                multiLabel: multiLabel,
                                passengerAddType: 'adults',
                                orderTotal: (orderBase.orderBaseAdults * adults) + (orderBase.orderBaseChildren * children) + (orderBase.orderBaseBabies * babies)
                            },
                            methods: {
                                calcTotalPrice: function(basePriceObj) {
                                    this.servicesPrice = this.calcServicePrices();
                                    this.orderBase = basePriceObj || this.orderBase;
                                    this.orderTotal = (this.orderBase.orderBaseAdults * this.adults) + (this.orderBase.orderBaseChildren * this.children) + (this.orderBase.orderBaseBabies * this.babies) + this.servicesPrice;
                                    this.orderAdvance = Math.round(this.orderTotal * this.orderAdvanceRatio);
                                },
                                calcServicePrices: function() {
                                    servicesPrice = 0;
                                    for (var i = 0; i < this.orderServices.length; i++) {
                                        if (this.orderServices[i].set) {
                                            this.calcService(i);
                                            servicesPrice = servicesPrice + this.orderServices[i].priceTotal;
                                        }
                                    }
                                    return servicesPrice;
                                },
                                calcService: function(index) {
                                    if (this.orderServices[index].multiply) {
                                        this.orderServices[index].passengers = this.passengers;
                                        this.orderServices[index].priceTotal = (this.orderServices[index].priceAdults * this.adults) + (this.orderServices[index].priceChildren * this.children) + (this.orderServices[index].priceBabies * this.babies);
                                    } else {
                                        this.orderServices[index].passengers = 1;
                                        this.orderServices[index].priceTotal = this.orderServices[index].priceAdults;
                                    }
                                },
                                setService: function(index) {
                                    this.orderServices[index].set = !this.orderServices[index].set;
                                    this.calcService(index);
                                    this.calcTotalPrice();
                                    calcOrder();
                                },
                                addPerson(type) {
                                    this[type]++;
                                    this.passengers++;
                                    this.passengerLabel = this.multiLabel;
                                    this.calcTotalPrice();
                                    calcOrder();
                                },
                                removePerson(type) {
                                    if (this[type] > 0) {
                                        if (this.passengers > 1) {
                                            if (this.passengers == 2) {
                                                this.passengerLabel = this.singleLabel;
                                            } else {
                                                this.passengerLabel = this.multiLabel;
                                            }
                                            this[type]--;
                                            this.passengers--;
                                            this.calcTotalPrice();
                                            calcOrder();
                                        }
                                    }
                                },
                                addRow(type) {
                                    var $clone = $passengerClone.clone(),
                                        dateMax = moment().toDate(),
                                        calName = "$birthCal" + this.passengers,
                                        passengerLabel = 'Suaugęs';
                                    this.passengers++;
                                    this[type]++;
                                    $clone.removeAttr('data-passenger-clone').removeClass('is-clone');
                                    $clone.find('[data-passenger-count]').text(this[type]);
                                    if (this.passengerAddType == 'adults') {
                                        passengerLabel = 'Suaugęs';
                                    } else if (this.passengerAddType == 'children') {
                                        passengerLabel = 'Vaikas';
                                    } else if (this.passengerAddType == 'babies') {
                                        passengerLabel = 'Kūdikis';
                                    }
                                    $clone.attr('data-passenger-type', this.passengerAddType);
                                    $clone.find('[data-passenger-label]').text(passengerLabel);

                                    $clone.appendTo('[data-passenger-clone-cont]');
                                    loadBirthCalendars();
                                    $('[data-selectize-gender]').each(function() {
                                        $(this).selectize({
                                            create: false,
                                            persist: false,
                                            create: function(input) {
                                                return {
                                                    value: input,
                                                    text: input
                                                }
                                            },
                                            onDropdownOpen: function() {
                                                this.blur();
                                            }
                                        });
                                    });
                                    this.calcTotalPrice();
                                    calcOrder();
                                }
                            },
                            beforeCreate: function() {},
                            mounted: function() {
                                console.log('mounted:');
                                console.log(this.orderAdvance);
                            }
                        }).$mount('[data-v="app"]');
                        $(document).on('click', '[data-remove-row]', function() {
                            var $parent = $(this).closest('[data-passenger-type]'),
                                passengerType = $parent.attr('data-passenger-type'),
                                tmpAdults = 0,
                                tmpChildren = 0,
                                tmpBabies = 0;
                            if (passengerType == 'adults') {
                                ncApp.calcApp.adults--;
                            } else if (passengerType == 'children') {
                                ncApp.calcApp.children--;
                            } else if (passengerType == 'babies') {
                                ncApp.calcApp.babies--;
                            }
                            ncApp.calcApp.passengers--;
                            $parent.remove();
                            ncApp.calcApp.calcTotalPrice();
                            calcOrder();
                            $('[data-passenger-type]').each(function() {
                                var type = $(this).attr('data-passenger-type');
                                if (type == 'adults') {
                                    tmpAdults++;
                                    $(this).find('[data-passenger-count]').text(tmpAdults);
                                }
                                if (type == 'children') {
                                    tmpChildren++;
                                    $(this).find('[data-passenger-count]').text(tmpChildren);
                                }
                                if (type == 'babies') {
                                    tmpBabies++;
                                    $(this).find('[data-passenger-count]').text(tmpBabies);
                                }
                            });
                        });
                    }
                    if ($('[data-form-date]').length) {
                        appendLib('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js', function() {
                            appendLib('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/locale/lt.js', function() {
                                appendLib('//cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js', function() {
                                    if ($('[data-form-date-from]').length) {
                                        loadRangeCalendars();
                                    }
                                    if ($('[data-form-birthdate]').length) {
                                        loadBirthCalendars();
                                    }
                                });
                            });
                        });
                    }
                    if ($('[data-selectize]').length) {
                        appendLib('//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.3/js/standalone/selectize.min.js', function() {
                            if ($('[data-selectize-gender]').length) {
                                $('[data-selectize-gender]').each(function() {
                                    $(this).selectize({
                                        create: false,
                                        persist: false,
                                        create: function(input) {
                                            return {
                                                value: input,
                                                text: input
                                            }
                                        },
                                        onDropdownOpen: function() {
                                            this.blur();
                                        }
                                    });
                                });
                            }
                            if ($('[data-selectize-passenger-type]').length) {
                                $('[data-selectize-passenger-type]').each(function() {
                                    $(this).selectize({
                                        create: false,
                                        persist: false,
                                        create: function(input) {
                                            return {
                                                value: input,
                                                text: input
                                            }
                                        },
                                        onDropdownOpen: function() {
                                            this.blur();
                                        },
                                        onChange: function(value) {
                                            ncApp.calcApp.passengerAddType = value;
                                        }
                                    });
                                });
                            }
                            if ($('[data-selectize-search]').length) {
                                var $selObj = $('[data-selectize-search]'),
                                    $selParent = $selObj.closest('[data-selectize-parent]'),
                                    $selPrepend = $selParent.find('[data-selectize-prepend]'),
                                    $selAppend = $selParent.find('[data-selectize-append]'),
                                    $selectizer,
                                    selectCtrl,
                                    itemCount = 0;
                                getSearchCookie();
                                $selectizer = $selObj.selectize({
                                    plugins: ['remove_button'],
                                    delimiter: ',',
                                    hideSelected: false,
                                    create: true,
                                    createOnBlur: true,
                                    closeAfterSelect: false,
                                    render: {
                                        option: function(data, escape) {
                                            return '<div class="search__form-option" data-search-option>\
                                                        <span class="search__form-option-box"></span>\
                                                        <span class="search__form-option-text">' + escape(data.text) + '</span>\
                                                    </div>';
                                        },
                                        item: function(data, escape) {
                                            return '<div class="search__form-select-item">' + escape(data.text) + '</div>';
                                        },
                                        option_create: function(data, escape) {
                                            return '<div class="create">Pridėti <strong>' + escape(data.input) + '</strong>&hellip;</div>';
                                        }
                                    },
                                    onInitialize: function() {
                                        $listObj = $selParent.find('.selectize-dropdown');
                                        $selAppend.removeClass('is-hidden');
                                        $selPrepend.removeClass('is-hidden');
                                        $listObj.append($selAppend);
                                        $listObj.prepend($selPrepend);
                                    },
                                    onDropdownOpen: function() {
                                        if (!$('[data-search].is-active').length && isMob) {
                                            this.blur();
                                        }
                                        $('[data-search-option]').click(function() {
                                            var val = $(this).attr('data-value');
                                            setTimeout(function() {
                                                selectCtrl.removeItem(val, false);
                                            }, 1);
                                        });
                                    },
                                    onOptionAdd: function() {
                                        console.log('sel sel');
                                    }
                                });
                                selectCtrl = $selectizer[0].selectize;
                                $('[data-selectize-history-item]').on('click', function() {
                                    var selVal = $(this).attr('data-selectize-history-item'),
                                        selText = $(this).text();
                                    selectCtrl.addOption({
                                        value: selVal,
                                        text: selText
                                    });
                                    selectCtrl.createItem(selVal);
                                    selectCtrl.refreshItems();
                                });
                                $('[data-search-form-clear]').on('click', function() {
                                    selectCtrl.clear();
                                    itemCount = 0;
                                    $('[data-value]').removeClass('selected');
                                    selectCtrl.refreshItems();
                                    $('[data-search-form-btn]').removeClass('is-visible');
                                });
                                $('[data-search-form-add]').on('click', function() {
                                    selectCtrl.close();
                                    setSearchCookie();
                                });
                                selectCtrl.on('item_remove', function(value) {
                                    $('[data-value="' + value + '"]').removeClass('selected');
                                    selectCtrl.refreshItems();
                                    itemCount--;
                                });
                                selectCtrl.on('item_add', function(value) {
                                    itemCount++;
                                });
                                selectCtrl.on('change', function() {
                                    setTimeout(function() {
                                        $('[data-search-form-add-counter]').text(itemCount);
                                        itemCount > 0 ? $('[data-search-form-btn]').addClass('is-visible') : $('[data-search-form-btn]').removeClass('is-visible');
                                    }, 0);
                                });
                                selectCtrl.on('option_add', function(value) {
                                    itemCount++;
                                });
                            }
                            if ($('[data-selectize-checkout]').length || $('[data-order-tour]').length) {
                                calcOrder();
                            }
                        });
                    }
                    if ($('[data-noui-slider]').length) {
                        appendLib('//cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js', function() {
                            initRangeSliders();
                        });
                    }
                    if ($('form').length) {
                        appendLib('//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.4.4/parsley.min.js', function() {
                            Parsley.addMessages('lt', {
                                defaultMessage: "Šis įrašas neteisingas.",
                                type: {
                                    email: "Įveskite teisingą telefono el. paštą.",
                                    url: "Šis įrašas nėra teisingas url.",
                                    number: "Šis įrašas nėra skaičius.",
                                    integer: "Šis įrašas nėra sveikasis skaičius.",
                                    digits: "Šis įrašas turi būti skaičius.",
                                    alphanum: "Šis įrašas turi būti iš skaičių ir raidžių."
                                },
                                notblank: "Šis įrašas negali būti tuščias.",
                                required: "Šis įrašas yra privalomas",
                                pattern: "Įveskite teisingą telefono numerį.",
                                min: "Ši vertė turi būti didesnė arba lygi %s.",
                                max: "Ši vertė turi būti mažesnė arba lygi %s.",
                                range: "Ši vertė turi būti tarp %s ir %s.",
                                minlength: "Šis įrašas per trumpas. Jis turi turėti %s simbolius arba daugiau.",
                                maxlength: "Šis įrašas per ilgas. Jis turi turėti %s simbolius arba mažiau.",
                                length: "Šio įrašo ilgis neteisingas. Jis turėtų būti tarp %s ir %s simbolių.",
                                mincheck: "Jūs turite pasirinkti bent %s pasirinkimus.",
                                maxcheck: "Jūs turite pasirinkti ne daugiau %s pasirinkimų.",
                                check: "Jūs turite pasirinkti tarp %s ir %s pasirinkimų.",
                                equalto: "Ši reikšmė turėtų būti vienoda."
                            });

                            Parsley.setLocale('lt');
                            $('form').each(function() {
                                $(this).parsley();
                            });
                        });
                    }
                    calcScrollFix();
                }

                $('[data-top]').bind('touchmove', function(e) {
                    var topActive = $(this).hasClass('is-active');
                    if (!topActive) {
                        e.preventDefault();
                    }
                });

                $(document).on('click', '[data-toggle]', function() {
                    var toggleString = $(this).data('toggle'),
                        $toggleObj = $('[data-' + toggleString + ']'),
                        $toggleParent = $(this).closest('[data-' + toggleString + ']');
                    if ($toggleParent.length) {
                        $toggleParent.hasClass('is-active') ? $toggleParent.removeClass('is-active') : $toggleParent.addClass('is-active');
                    } else {
                        $toggleObj.hasClass('is-active') ? $toggleObj.removeClass('is-active') : $toggleObj.addClass('is-active');
                    }
                    $(this).hasClass('is-active') ? $(this).removeClass('is-active') : $(this).addClass('is-active');
                });

                $(document).on('click', '[data-top-trigger]', function() {
                    if (isMob) {
                        $('[data-top]').hasClass('is-active') ? $('[data-top]').removeClass('is-active') : $('[data-top]').addClass('is-active');
                    }
                });

                $(document).on('click', '[data-submenu]', function() {
                    if (isMob) {
                        if ($(this).hasClass('is-active')) {
                            $('[data-submenu]').removeClass('is-active');
                        } else {
                            $('[data-submenu]').removeClass('is-active');
                            $(this).addClass('is-active');
                        }
                    }
                });

                $(document).on('click', '[data-modal-trigger]', function() {
                    var title = $(this).attr('data-modal-trigger'),
                        start = $(this).index();
                    openModal(title, start);
                });

                $(document).on('click', '[data-service-trigger]', function() {
                    var $serviceObj = $(this).closest('[data-service]'),
                        serviceActive = $serviceObj.hasClass('is-set'),
                        serviceName = $serviceObj.data('service'),
                        serviceLabel = $serviceObj.find('[data-service-label]').text(),
                        serviceMultiplier = $serviceObj.data('service-multiply'),
                        servicePrice = $serviceObj.find('[data-service-price]').text(),
                        $orderServiceObj = {},
                        orderMultiplier = Number($('[data-order-multiplier]').text()) || Number($('[data-order-multiplier]').val());
                    if (!serviceMultiplier) {
                        orderMultiplier = 1;
                    }
                    if (!serviceActive) {
                        $serviceObj.addClass('is-set');
                        $orderServiceObj = $('[data-order-service="none"]').clone();
                        $orderServiceObj.attr('data-order-service', serviceName)
                            .addClass('is-set')
                            .find('[data-order-service-label]')
                            .text(serviceLabel);
                        $orderServiceObj.find('[data-order-service-base]').text(servicePrice);
                        $orderServiceObj.find('[data-order-service-multiplier]').text(orderMultiplier);
                        $('[data-order-services]').append($orderServiceObj);
                        // } else {
                        //     $serviceObj.removeClass('is-set');
                        //     $('[data-order-service="' + serviceName + '"]').remove();
                    }
                    // if ($('[data-order-tour]').length) {
                    // $('[data-form-passenger-action]').unbind('click');
                    // calcTour();
                    // }
                    if ($('[data-selectize-checkout]').length || $('[data-order-tour]').length) {
                        calcOrder();
                    }
                });

                $(document).on('click', '[data-order-service-clear]', function() {
                    var $serviceOrderObj = $(this).closest('[data-order-service]'),
                        serviceName = $serviceOrderObj.data('order-service'),
                        $serviceObj = $('[data-service="' + serviceName + '"]');
                    $serviceObj.removeClass('is-set');
                    $('[data-order-service="' + serviceName + '"]').remove();
                    calcOrder();
                });
                $(document).on('click', '*', function(e) {
                    // alert($(e.target).attr('class'));
                    if (!$(e.target).closest('[data-form-passengers]').length) {
                        $('[data-form-passengers]').removeClass('is-active');
                    }
                });

                $(document).on('click', '[data-modal]', function(e) {
                    if (!$(e.target).closest('[data-modal-close-prevent]').length) {
                        $('[data-modal]').removeClass('is-active');
                        $('body').removeClass('is-fixed');
                        $('[data-fixer]').removeAttr('style');
                    }
                });

                $(document).on('click', '[data-form-passengers-trigger]', function() {
                    var $passParent = $('[data-form-passengers]'),
                        passParentActive = $passParent.hasClass('is-active');
                    if (passParentActive) {
                        $passParent.removeClass('is-active');
                    } else {
                        $passParent.addClass('is-active');
                    }
                });

                $(document).on('click', '[data-form-passenger-action]', function() {
                    var $passObj = $(this).closest('[data-form-passenger]'),
                        $passTotalValObj = $('[data-form-passenger-count]'),
                        $passParentObj = $passObj.closest('[data-form-passengers]'),
                        $passTotalObj = $passParentObj.find('[data-form-passenger-total]'),
                        $passValObj = $passObj.find('[data-form-passenger-val]'),
                        passAction = $(this).attr('data-form-passenger-action'),
                        passSingleText = $passTotalObj.attr('data-form-passenger-single-text'),
                        passMultiText = $passTotalObj.attr('data-form-passenger-multi-text'),
                        passCount = Number($passValObj.text()),
                        passTotal = Number($passTotalValObj.val()),
                        passTotalText;
                    if (passAction === 'add') {
                        passCount++;
                        passTotal++;
                    } else if (passAction === 'remove' && passCount > 0 && passTotal > 1) {
                        passCount--;
                        passTotal--;
                    }
                    if (passTotal === 1) {
                        passTotalText = passTotal + ' ' + passSingleText;
                    } else {
                        passTotalText = passTotal + ' ' + passMultiText;
                    }
                    $passObj.find('[data-form-passenger-val]').text(passCount);
                    $('[data-form-passenger-count]').val(passTotal);
                    $passTotalObj.text(passTotalText);
                    $('[data-order-service]').each(function() {
                        var serviceName = $(this).attr('data-order-service'),
                            serviceMultiply = $('[data-service="' + serviceName + '"]').attr('data-service-multiply');
                        if (serviceMultiply !== 'false' && serviceMultiply !== 'undefined') {
                            $(this).find('[data-order-service-multiplier]').text(passTotal);
                        }
                    });
                    calcOrder();
                });

                $(document).on('click', '[data-search]', function(e) {
                    if ($(e.target).closest('[data-search-close]').length) {
                        $(this).removeClass('is-active');
                    } else if (!$(e.target).closest('[data-search-submit]').length) {
                        $(this).addClass('is-active');
                    }
                });

                $(document).on('click', '[data-tour-toggle]', function() {
                    var tourActive = $('[data-tour-side]').hasClass('is-active'),
                        bodyW = $('body').width(),
                        bodyDiff = 0;
                    if (tourActive) {
                        $('[data-fixer]').removeClass('is-fixed');
                        $('body').removeClass('is-fixed');
                        $('[data-tour-side]').removeClass('is-active');
                        $('[data-top]').removeClass('is-hidden');
                        $('[data-tour-toggle]').addClass('is-active');
                    } else {
                        $('[data-fixer]').addClass('is-fixed');
                        $('body').addClass('is-fixed');
                        $('[data-top]').addClass('is-hidden');
                        $('[data-tour-side]').addClass('is-active');
                        $('[data-tour-toggle]').removeClass('is-active');
                        calcOrder();
                    }
                });

                $(document).on('click', '[data-modal-close]', function() {
                    $('[data-modal]').removeClass('is-active');
                    $('body').removeClass('is-fixed');
                    $('[data-fixer]').removeAttr('style');
                });

                $(document).on('click', '[data-gallery-switch]', function() {
                    var $switchParent = $(this).closest('[data-gallery-thumbs]'),
                        thumbsHidden = $switchParent.hasClass('is-hidden');
                    if (!thumbsHidden) {
                        $switchParent.addClass('is-hidden');

                    } else {
                        $switchParent.removeClass('is-hidden');

                    }

                    console.log(thumbsHidden);
                });

                $(document).on('submit', '[data-search]', function(e) {
                    e.preventDefault();
                    setSearchCookie();
                    if (isMob && !$('[data-search]').hasClass('is-active')) {
                        e.preventDefault();
                        $('[data-search]').addClass('is-active');
                    }
                });
            });
        });
    });
});

function openModal(title, start) {
    var $modalObj = $('[data-modal="' + title + '"]'),
        bodyW = $('body').width(),
        bodyDiff = 0;
    $('body').addClass('is-fixed');
    bodyDiff = bodyW - $('body').width();
    $('[data-fixer]').css({
        paddingRight: -bodyDiff
    });
    $modalObj.addClass('is-active');
    if ($modalObj.find('.js-swiper').length) {
        initSwiper(title, start);
    }
}

function initSwiper(title, start) {
    if (!window.Swiper) {
        appendLib('//cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js', function() {
            initSwiper(title, start);
        });
    } else {
        $parent = $('[data-modal=' + title + ']'),
            name = $parent.data('modal'),
            swiperName = name + 'Swiper',
            $swiperObj = $parent.find('.js-swiper'),
            $thumbs = $parent.find('.js-swiper-thumb');
        if (window[swiperName]) {
            window[swiperName].update(true, true);
            window[swiperName].slideTo(start + 1, 0);
        } else {
            window[swiperName] = new Swiper($swiperObj, {
                loop: true,
                initialSlide: start,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
            $thumbs.eq(start).addClass('is-active');
        }
        window[swiperName].on('transitionEnd', function() {
            var activeSlide = window[swiperName].activeIndex - 1,
                slideCount = window[swiperName].slides.length - 2;
            if (activeSlide == slideCount) {
                activeSlide = 0;
            } else if (activeSlide < 0) {
                activeSlide = slideCount - 1;
            }
            $('.js-swiper-thumb').removeClass('is-active');
            $thumbs.eq(activeSlide).addClass('is-active');
        });
        $(document).on('click', '.js-swiper-thumb', function() {
            $('.js-swiper-thumb').removeClass('is-active');
            window[swiperName].slideTo($(this).index() + 1);
            $(this).addClass('is-active');
        });
    }
}

function loadBirthCalendars() {
    var i = 0,
        parent,
        calName;
    $('[data-form-birthdate]').each(function() {
        var dateMax = moment().toDate(),
            calName = "$birthCal" + i;
        // if (isMob) {
        //     parent = $(this).closest('[data-form-parent]');
        // } else {
        parent = $(this).closest('[data-form-parent]');
        // }
        window[calName] = $(this).daterangepicker({
            locale: {
                monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],
                format: 'YYYY-MM-DD'
            },
            parentEl: parent,
            singleDatePicker: true,
            firstDay: 1,
            showDropdowns: true
                // changeMonth: true,
                // changeYear: true,
                // maxDate: dateMax
        });
        i = i + 1;
    });
}

function loadRangeCalendars() {
    var dateFrom = moment().toDate(),
        dateTo = moment().add(1, 'months').toDate(),
        fromSet = false,
        toSet = false,
        $rangeFromCalendar = $('[data-form-date-from]').daterangepicker({
            locale: {
                monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],
                firstDay: 1,
                format: 'YYYY-MM-DD'
            },
            showDropdowns: true,
            startDate: dateFrom,
            endDate: dateTo,
            minDate: dateFrom,
            parentEl: '[data-form-date-parent]',
            autoUpdateInput: false,
            singleDatePicker: isMob,
            autoApply: true
        }),
        $rangeToCalendar = $('[data-form-date-to]').daterangepicker({
            locale: {
                monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],
                format: 'YYYY-MM-DD'
            },
            firstDay: 1,
            showDropdowns: true,
            startDate: dateFrom,
            endDate: dateTo,
            minDate: dateFrom,
            parentEl: '[data-form-date-parent]',
            autoUpdateInput: false,
            singleDatePicker: isMob,
            autoApply: true
        });
    $rangeFromCalendar.on('apply.daterangepicker', function(e, picker) {
        dateFrom = picker.startDate.format('YYYY-MM-DD');
        $('[data-form-date-from]').val(moment(dateFrom).format('YYYY-MM-DD'));
        $rangeFromCalendar.data('daterangepicker').setStartDate(moment(dateFrom).format('YYYY-MM-DD'));
        if (isMob && !toSet) {
            $rangeToCalendar.data('daterangepicker').setEndDate(moment(dateTo).format('YYYY-MM-DD'));
        }
        if (!isMob) {
            dateTo = picker.endDate.format('YYYY-MM-DD');
            $('[data-form-date-to]').val(moment(dateTo).format('YYYY-MM-DD'));
            $rangeToCalendar.data('daterangepicker').setEndDate(moment(dateTo).format('YYYY-MM-DD'));
        }
        fromSet = true;
    });
    $rangeToCalendar.on('apply.daterangepicker', function(e, picker) {
        if (!isMob) {
            dateFrom = picker.startDate.format('YYYY-MM-DD');
            $('[data-form-date-from]').val(moment(dateFrom).format('YYYY-MM-DD'));
        }
        dateTo = picker.endDate.format('YYYY-MM-DD');
        $('[data-form-date-to]').val(moment(dateTo).format('YYYY-MM-DD'));
        $rangeFromCalendar.data('daterangepicker').setStartDate(moment(dateFrom).format('YYYY-MM-DD'));
        $rangeFromCalendar.data('daterangepicker').setEndDate(moment(dateTo).format('YYYY-MM-DD'));
        toSet = true;
    });
    $('[data-search-dates-clear]').on('click', function() {
        $('[data-form-date-from]').val('');
        $('[data-form-date-to]').val('');

    });
}

function calcOrder() {
    var $orderSelect = $('[data-selectize-checkout]'),
        $datesSelect = $('[data-selectize-dates]');
    if ($orderSelect.length) {
        if (orderSelectizer) {
            orderSelectizer[0].selectize.destroy();
        }
        orderSelectizer = $orderSelect.selectize({
            valueField: 'val',
            labelField: 'val',
            searchField: 'val',
            placeholder: 'Pasirinkite apmokėjimą',
            options: [
                { val: ncApp.calcApp.orderAdvance },
                { val: ncApp.calcApp.orderTotal }
            ],
            create: false,
            onDropdownOpen: function() {
                this.blur();
            },
        });
        orderSelectizer[0].selectize.setValue(ncApp.calcApp.orderAdvance);
    }
    if ($datesSelect.length) {
        if (datesSelectizer) {
            datesSelectizer[0].selectize.destroy();
        }
        datesSelectizer = $datesSelect.selectize({
            hideSelected: true,
            create: false,
            createOnBlur: false,
            closeAfterSelect: true,
            render: {
                option: function(data, escape) {
                    var optionText = escape(data.text),
                        optionVal = escape(data.value),
                        optionArr = optionVal.split(' '),
                        optionObj = {
                            'orderBaseAdults': optionArr[0],
                            'orderBaseChildren': optionArr[1],
                            'orderBaseBabies': optionArr[2]
                        },
                        optionString = optionText.replace(/\u20ac/g, '');
                    optionString = optionString.replace(optionObj.orderBaseAdults, '');
                    optionVal = optionObj.orderBaseAdults;
                    return '<div class="side__dates-option">' + optionString + '<b>\u20ac ' + optionVal + '</b></div>';
                },
                item: function(data, escape) {
                    var optionText = escape(data.text),
                        optionVal = escape(data.value),
                        optionArr = optionVal.split(' '),
                        optionObj = {
                            'orderBaseAdults': optionArr[0],
                            'orderBaseChildren': optionArr[1],
                            'orderBaseBabies': optionArr[2]
                        },
                        optionString = optionText.replace(/\u20ac/g, '');
                    optionString = optionString.replace(optionObj.orderBaseAdults, '');
                    optionVal = optionObj.orderBaseAdults;
                    return '<div class="search__form-date-select-item">' + optionString + '<b>\u20ac ' + optionVal + '</b></div>';
                }
            },
            onChange: function(data) {
                var optionVal = data,
                    optionArr = optionVal.split(' '),
                    optionObj = {
                        'orderBaseAdults': Number(optionArr[0]),
                        'orderBaseChildren': Number(optionArr[1]),
                        'orderBaseBabies': Number(optionArr[2])
                    };
                console.log(optionObj);
                ncApp.calcApp.calcTotalPrice(optionObj);
            },
            onDropdownOpen: function() {
                this.blur();
            }
        });
        datesSelectizer[0].selectize.setValue(ncApp.calcApp.orderBase.orderBaseAdults + ' ' + ncApp.calcApp.orderBase.orderBaseChildren + ' ' + ncApp.calcApp.orderBase.orderBaseBabies);
    }
}

function initRangeSliders() {
    var $nouiParent,
        $nouiStartVal,
        $nouiEndVal,
        nouiSnapVals,
        nouiName,
        nouiMax,
        nouiMin,
        nouiStart,
        nouiStep,
        nouiEnd;
    $('[data-noui-slider]').each(function() {
        $nouiParent = $(this).closest('[data-noui-parent]');
        $nouiStartVal = $nouiParent.find('[data-noui-start-val]');
        $nouiEndVal = $nouiParent.find('[data-noui-end-val]');
        $nouiMinInput = $nouiParent.find('[data-noui-min-input]');
        $nouiMaxInput = $nouiParent.find('[data-noui-max-input]');
        $nouiEndVal = $nouiParent.find('[data-noui-end-val]');
        nouiName = $(this).attr('data-noui-slider');
        nouiMax = Number($(this).attr('data-noui-max'));
        nouiMin = Number($(this).attr('data-noui-min'));
        nouiStart = Number($(this).attr('data-noui-start'));
        nouiEnd = Number($(this).attr('data-noui-end'));
        nouiStep = Number($(this).attr('data-noui-step'));
        noUiSlider.create($(this)[0], {
            start: [nouiStart, nouiEnd],
            step: nouiStep,
            connect: true,
            range: {
                'min': nouiMin,
                'max': nouiMax
            }
        });
        $(this)[0].noUiSlider.on('update', function(values, handle) {
            $nouiMinInput.val(Math.round(values[0], 0));
            $nouiMaxInput.val(Math.round(values[1], 0));
            $nouiStartVal.text(Math.round(values[0], 0));
            $nouiEndVal.text(Math.round(values[1], 0));

        });

    });
}

function setSearchCookie() {
    var $select = $('[data-selectize-search]'),
        $options = $select.find('option'),
        duplicates,
        optionObj = {};
    searchCookie = Cookies.getJSON('searchCookie') || [];
    for (var i = 0; i < $options.length; i++) {
        optionObj = {};
        optionObj.name = $options.eq(i).text();
        optionObj.val = $options.eq(i).val();
        duplicates = $.grep(searchCookie, function(obj, i) {
            return obj.val != optionObj.val;
        });
        if (duplicates.length == searchCookie.length) {
            searchCookie.push(optionObj);
        }
    }
    Cookies.set('searchCookie', searchCookie);
}

function getSearchCookie() {
    searchCookie = Cookies.getJSON('searchCookie');
    if (searchCookie !== undefined) {
        var itemCount = searchCookie.length,
            $searchHistoryCont = $('[data-selectize-history-items]');
        $searchHistoryCont.empty();
        while (itemCount--) {
            var searchItem = '<span class="search__history-item" data-selectize-history-item="' + searchCookie[itemCount].val + '">' + searchCookie[itemCount].name + '</span>\n',
                $searchItem = $(searchItem);
            $searchHistoryCont.append($searchItem);
        }
    }
}

var setLayouts = debounce(function() {
    checkMob();
    calcScrollFix();
}, 150);

function initSwipers() {
    var $parent,
        $thumbs,
        name,
        swiperName,
        activeSlide;
    $.each($('.js-swiper'), function() {
        $parent = $(this).closest('[data-modal]');
        name = $parent.data('modal');
        swiperName = name + 'Swiper';
        window[swiperName] = new Swiper($(this), {
            loop: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
        });
    });
}

// MOBILE DETECT
function checkMob(maxW) {
    maxW = maxW || '768'; // default value is 768
    winW = $(window).outerWidth();
    winH = $(window).outerHeight();
    docH = $(document).outerHeight();
    isTouch = ((typeof(window.ontouchstart) !== "undefined")); // true of false
    isMob = (maxW >= winW); // true of false
    if (isTouch) {
        $('html').removeClass('no-touch');
    } else {
        $('html').addClass('no-touch');

    }
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments,
            later = function() {
                timeout = null;
                if (!immediate) {
                    func.apply(context, args);
                }
            },
            callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
            func.apply(context, args);
        }
    };
}

function appendLib(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) { //IE
        script.onreadystatechange = function() {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function() {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}
