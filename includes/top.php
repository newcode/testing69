<div class="top" data-top data-fixer>
    <div class="wrap">
        <div class="top__logo">
            <div class="top__img">
                <?php include 'img/logo.svg'; ?>
            </div>
            <a href="" class="top__link"></a>
        </div>
        <div class="top__mob-title">Egzotinės kelionės</div>
        <div class="top__nav nav">
            <div class="nav__trigger" data-top-trigger>
                <div class="nav__navicon"></div>
            </div>
            <div class="nav__mob-title">Meniu</div>
            <div class="nav__main-menu main-menu">
                <ul class="main-menu__list">
                    <li class="main-menu__item">
                        <a href="" class="main-menu__trigger">Skrydžiai</a>
                    </li>
                    <li class="main-menu__item has-submenu" data-submenu>
                        <span class="main-menu__trigger">Kelionės</span>
                        <ul class="main-menu__submenu">
                            <li class="main-menu__subitem">
                                <a href="" class="main-menu__sublink">Poilsinės kelionės</a>
                            </li>
                            <li class="main-menu__subitem">
                                <a href="" class="main-menu__sublink">Pažintinės kelionės</a>
                            </li>
                            <li class="main-menu__subitem">
                                <a href="" class="main-menu__sublink">Mišrios kelionės</a>
                            </li>
                            <li class="main-menu__subitem">
                                <a href="" class="main-menu__sublink">Įdomios kelionės</a>
                            </li>
                        </ul>
                    </li>
                    <li class="main-menu__item">
                        <a href="" class="main-menu__trigger">Automobilių nuoma</a>
                    </li>
                    <li class="main-menu__item">
                        <a href="" class="main-menu__trigger">Draudimas</a>
                    </li>
                    <li class="main-menu__item">
                        <a href="" class="main-menu__trigger">Vizos</a>
                    </li>
                </ul>
            </div>
            <div class="nav__info-menu info-menu">
                <ul class="info-menu__list">
                    <li class="info-menu__item" data-submenu>
                        <span class="info-menu__trigger">Pagalba</span>
                        <ul class="info-menu__help-list">
                            <li class="info-menu__help-item">
                                <a href="" class="info-menu__help-link">Kelionių kryptys</a>
                            </li>
                            <li class="info-menu__help-item">
                                <a href="" class="info-menu__help-link">Sąlygos</a>
                            </li>
                            <li class="info-menu__help-item">
                                <a href="" class="info-menu__help-link">D. U. K.</a>
                            </li>
                            <li class="info-menu__help-item">
                                <a href="" class="info-menu__help-link">Kontaktai</a>
                            </li>
                            <li class="info-menu__help-item info-menu__help-item--info">
                                <a href="tel:+370 (700) 22 820" class="info-menu__help-phone">+370 (700) 22 820</a>
                                <span class="info-menu__help-desc">Pagalba telefonu visą parą 24/7</span>
                            </li>
                        </ul>
                    </li>
                    <li class="info-menu__item" data-submenu>
                        <span class="info-menu__trigger info-menu__trigger--airguru">Apie <em>Air<i>Guru</i></em></span>
                        <ul class="info-menu__about-grid">
                            <li class="info-menu__about-item">
                                <div class="info-menu__about-image">
                                    <?php include 'img/clients.svg'; ?>
                                </div>
                                <div class="info-menu__about-label">2500 aptarnautų klientų</div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                            <li class="info-menu__about-item">
                                <div class="info-menu__about-image">
                                    <?php include 'img/insurance.svg'; ?>
                                </div>
                                <div class="info-menu__about-label">€ 50.000 draudimas</div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                            <li class="info-menu__about-item">
                                <div class="info-menu__about-image">
                                    <?php include 'img/stipriausi.svg'; ?>
                                </div>
                                <div class="info-menu__about-label">Stipriausi Lietuvoje 2015</div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                            <li class="info-menu__about-item">
                                <div class="info-menu__about-image">
                                    <?php include 'img/continents.svg'; ?>
                                </div>
                                <div class="info-menu__about-label">Kelionės į visus žemynus</div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                            <li class="info-menu__about-item">
                                <div class="info-menu__about-rating">4.8</div>
                                <div class="info-menu__about-stars">
                                    <div class="info-menu__about-star">
                                        <?php include 'img/star.svg'; ?>
                                    </div>
                                    <div class="info-menu__about-star">
                                        <?php include 'img/star.svg'; ?>
                                    </div>
                                    <div class="info-menu__about-star">
                                        <?php include 'img/star.svg'; ?>
                                    </div>
                                    <div class="info-menu__about-star">
                                        <?php include 'img/star.svg'; ?>
                                    </div>
                                    <div class="info-menu__about-star">
                                        <?php include 'img/star.svg'; ?>
                                    </div>
                                </div>
                                <div class="info-menu__about-label">Keliautojų vertinimas</div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                            <li class="info-menu__about-item info-menu__about-item--more">
                                <div class="info-menu__about-more">
                                    DAUGIAU
                                    <?php include 'img/more.svg'; ?>
                                </div>
                                <a href="" class="info-menu__about-link"></a>
                            </li>
                        </ul>
                    </li>
                    <li class="info-menu__mob-help">
                        <span class="info-menu__mob-text">Pagalba visą parą</span>
                        <a href="tel:+370 (700) 22 820" class="info-menu__mob-phone">+370 (700) 22 820 </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
