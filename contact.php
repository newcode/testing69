<?php include 'includes/head.php'; ?>
<?php include 'includes/top.php'; ?>
<div class="page page--contact" data-fixer>
    <div class="page__content">
        <div class="wrap">
            <form action="" class="form contact">
                <h2 class="contact__title">Susisiekite su ekspertu</h2>
                <div class="contact__subtitle">Netinka datos ar norėtum pakoreguoti kelionės maršrutą? Susisiek, mes atviri pasiūlymams!</div>
                <div class="form__row contact__form-row" data-form-parent>
                    <div class="form__item contact__form-item contact__form-item--sm" data-form-item>
                        <label class="form__label contact__form-label">
                            <span class="form__label-content contact__form-label-content">Vardas</span>
                            <input type="text" class=" form__text form__input contact__form-text contact__form-input" name="contact_name" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                    <div class="form__item contact__form-item contact__form-item--sm" data-form-item>
                        <label class="form__label contact__form-label">
                            <span class="form__label-content contact__form-label-content">Telefonas</span>
                            <input type="text" class=" form__text form__input contact__form-text contact__form-input" name="contact_phone" placeholder="+370" data-form-input required pattern="/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{5})$/">
                        </label>
                    </div>
                    <div class="form__item contact__form-item contact__form-item--sm" data-form-item>
                        <label class="form__label contact__form-label">
                            <span class="form__label-content contact__form-label-content">El. paštas</span>
                            <input type="email" class=" form__text form__input contact__form-text contact__form-input" name="contact_email" placeholder="Pvz. Jonas" data-form-input required>
                        </label>
                    </div>
                </div>
                <div class="form__row contact__form-row" data-form-parent>
                    <div class="form__item contact__form-item" data-form-item>
                        <label class="form__label contact__form-label">
                            <span class="form__label-content contact__form-label-content">Tema</span>
                            <input type="text" class="form__text form__input contact__form-text contact__form-input" name="contact_topic" placeholder="Tema" data-form-input required>
                        </label>
                    </div>
                </div>
                <div class="form__row contact__form-row" data-form-parent>
                    <div class="form__item contact__form-item" data-form-item>
                        <label class="form__label contact__form-label">
                            <span class="form__label-content contact__form-label-content">Tekstas</span>
                            <textarea type="text" class="form__textarea form__input contact__form-textara contact__form-input" name="contact_content" rows="5" placeholder="Pvz. Jonas" data-form-input required></textarea>
                        </label>
                    </div>
                </div>
                <div class="form__row contact__form-row" data-form-parent>
                    <div class="form__item contact__form-item" data-form-item>
                            <button type="submit" class="form__submit form__input btn contact__form-submit contact__form-input" name="contact_submit" data-form-input required>Siųsti</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</div>
<?php include 'includes/foot.php'; ?>
